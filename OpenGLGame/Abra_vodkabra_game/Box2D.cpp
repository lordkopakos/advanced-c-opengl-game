#include "Box2D.h"

Box2D::Box2D()
{
}

void Box2D::init(b2World * world, 
	const glm::vec2 & position, 
	const glm::vec2 & dimensions, GameEngine::GLTexture texture, 
	const GameEngine::ColorRGBA8& color, bool fixed_rotation /*= false*/, 
	const glm::vec4& UV_rectangle/* = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f)*/)
{	
	b2BodyDef _body_def;
	_body_def.type = b2_dynamicBody;
	_body_def.position.Set(position.x, position.y);
	_body_def.fixedRotation = fixed_rotation;

	m_body = world->CreateBody(&_body_def);

	b2PolygonShape _box_shape;
	_box_shape.SetAsBox(dimensions.x / 2.0f, dimensions.y / 2.0f);

	b2FixtureDef _fixture_def;
	_fixture_def.shape = &_box_shape;
	_fixture_def.density = 1.0f;
	_fixture_def.friction = 0.3f;
	m_fixture = b2Fixture_ptr( m_body->CreateFixture(&_fixture_def));

	m_dimensions = dimensions;
	m_UV_rectangle = UV_rectangle;
	m_texture = texture;
	m_color = color;
}

void Box2D::draw(GameEngine::SpriteBatch& sprite_batch)
{
	glm::vec4 _dest_rect;
	_dest_rect.x = get_body()->GetPosition().x - get_dimensions().x / 2.0f;
	_dest_rect.y = get_body()->GetPosition().y - get_dimensions().y / 2.0f;
	_dest_rect.z = get_dimensions().x;
	_dest_rect.w = get_dimensions().y;
	sprite_batch.draw(_dest_rect, m_UV_rectangle, m_texture.id, 0.0f, get_color(), get_body()->GetAngle());
}

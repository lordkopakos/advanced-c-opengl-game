#pragma once

#include<GameEngine\Vertex.h>
#include<GameEngine\SpriteBatch.h>
#include<GameEngine\GLTexture.h>

#include<Box2D\Box2D.h>

#include<glm\glm.hpp>

using b2Fixture_ptr = std::shared_ptr<b2Fixture>;

class Box2D
{
public:
	Box2D();
	~Box2D()=default;

	void init(b2World* world,
		const glm::vec2& position,
		const glm::vec2& dimensions, 
		GameEngine::GLTexture texture, 
		const GameEngine::ColorRGBA8& color, 
		bool fixed_rotation = false, 
		const glm::vec4& UV_rectangle = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));

	void draw(GameEngine::SpriteBatch& sprite_batch);

	b2Body* get_body() const { return m_body; }
	b2Fixture_ptr get_fixture() const { return m_fixture; }
	const glm::vec2& get_dimensions() const { return m_dimensions; }
	const GameEngine::ColorRGBA8& get_color() const { return m_color; }

private:
	b2Body* m_body = nullptr;
	b2Fixture_ptr m_fixture = nullptr;
	glm::vec2 m_dimensions;
	glm::vec4 m_UV_rectangle;
	GameEngine::ColorRGBA8 m_color;
	GameEngine::GLTexture m_texture;
};


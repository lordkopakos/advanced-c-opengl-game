#include "Capsule2D.h"

void Capsule2D::init(b2World * world, const glm::vec2 & position, const glm::vec2 & dimensions,float density, float friction, bool fixed_rotation)
{
	m_dimensions = dimensions;

	b2BodyDef _body_def;
	_body_def.type = b2_dynamicBody;
	_body_def.position.Set(position.x, position.y);
	_body_def.fixedRotation = fixed_rotation;

	m_body = world->CreateBody(&_body_def);

	//Create Box
	b2PolygonShape _box_shape;
	_box_shape.SetAsBox(dimensions.x / 2.0f, (dimensions.y- dimensions.x) / 2.0f);

	b2FixtureDef _fixture_def;
	_fixture_def.shape = &_box_shape;
	_fixture_def.density = density;
	_fixture_def.friction = friction;
	m_fixtures[0] = b2Fixture_ptr(m_body->CreateFixture(&_fixture_def));

	//Create circles
	b2CircleShape _circle_shape;
	_circle_shape.m_radius = dimensions.x / 2.0f;

	b2FixtureDef _circle_def;
	_circle_def.shape = &_circle_shape;
	_circle_def.density = density;
	_circle_def.friction = friction;

	//Bottom circle
	_circle_shape.m_p.Set(0.0f, (-m_dimensions.y + dimensions.x) / 2.0f);
	m_fixtures[1] = b2Fixture_ptr(m_body->CreateFixture(&_circle_def));
	//Top circle
	_circle_shape.m_p.Set(0.0f, (m_dimensions.y - dimensions.x) / 2.0f);
	m_fixtures[2] = b2Fixture_ptr(m_body->CreateFixture(&_circle_def));
}

void Capsule2D::draw_debug(GameEngine::Debug_renderer & debug_renderer)
{
	GameEngine::ColorRGBA8 _color(255, 255, 255, 255);

	//Draw box
	glm::vec4 _dest_rect;
	_dest_rect.x = m_body->GetPosition().x - m_dimensions.x / 2.0f;
	_dest_rect.y = m_body->GetPosition().y - (m_dimensions.y - m_dimensions.x) / 2.0f;
	_dest_rect.z = m_dimensions.x;
	_dest_rect.w = m_dimensions.y - m_dimensions.x;

	debug_renderer.draw_box(_dest_rect, GameEngine::ColorRGBA8(255, 255, 255, 255), m_body->GetAngle());

	//Draw circles
	debug_renderer.draw_circle(glm::vec2(_dest_rect.x+m_dimensions.x/2.0f, _dest_rect.y), _color, m_dimensions.x/2.0f);
	debug_renderer.draw_circle(glm::vec2(_dest_rect.x + m_dimensions.x / 2.0f, _dest_rect.y + _dest_rect.w), _color, m_dimensions.x / 2.0f);
}

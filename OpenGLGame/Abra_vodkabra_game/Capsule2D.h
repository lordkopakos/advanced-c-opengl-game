#pragma once
#include<GameEngine\Debug_renderer.h>
#include<Box2D\Box2D.h>

#include<glm\glm.hpp>

#include<array>

using b2Fixture_ptr = std::shared_ptr<b2Fixture>;

class Capsule2D
{
public:
	Capsule2D() = default;
	
	void init(b2World * world, 
		const glm::vec2 & position, 
		const glm::vec2 & dimensions, 
		float density, 
		float friction, 
		bool fixed_rotation);

	void draw_debug(GameEngine::Debug_renderer& debug_renderer);

	b2Body* get_body() const { return m_body; }
	std::array<b2Fixture_ptr, 3> get_fixtures() const { return m_fixtures; }
	b2Fixture_ptr get_fixture(int index) const { return m_fixtures[index]; }
	const glm::vec2& get_dimensions() const { return m_dimensions; }

private:
	b2Body* m_body = nullptr;
	std::array<b2Fixture_ptr, 3> m_fixtures;
	glm::vec2 m_dimensions;
};


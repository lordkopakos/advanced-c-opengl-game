#include "Game_engine.h"
#include <GameEngine\Screen_list.h>


Game_engine::Game_engine()
{
}

void Game_engine::on_init()
{
}

void Game_engine::add_screen()
{
	m_mainmenu_screen = std::make_shared<Mainmenu_screen>(&m_window);
	m_gameplay_screen = std::make_shared<Gameplay_screen>(&m_window);
	m_lvl_editor_screen = std::make_shared<Lvl_editor_screen>(&m_window);

	m_screen_list->add_screen(m_mainmenu_screen);
	m_screen_list->add_screen(m_gameplay_screen);
	m_screen_list->add_screen(m_lvl_editor_screen);

	m_screen_list->set_screen(m_mainmenu_screen->get_index());
}

void Game_engine::on_exit()
{
}

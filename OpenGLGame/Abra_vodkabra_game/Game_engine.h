#pragma once

#include<GameEngine\IGame_engine.h>
#include"Gameplay_screen.h"
#include"Mainmenu_screen.h"
#include"Lvl_editor_screen.h"

class Game_engine :public GameEngine::IGame_engine
{
public:
	Game_engine();
	~Game_engine()=default;

	virtual void on_init() override;
	virtual void add_screen() override;
	virtual void on_exit() override;

private:

	std::shared_ptr < Gameplay_screen> m_gameplay_screen = nullptr;
	std::shared_ptr < Mainmenu_screen> m_mainmenu_screen = nullptr;
	std::shared_ptr < Lvl_editor_screen> m_lvl_editor_screen = nullptr;
};


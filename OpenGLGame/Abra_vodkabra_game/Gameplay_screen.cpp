#include "Gameplay_screen.h"
#include "Player.h"
#include "Light.h"
#include "Screen_indices.h"

#include <GameEngine\IGame_engine.h>
#include <GameEngine\ResourceManager.h>

#include <SDL\SDL.h>

#include <iostream>
#include <random>
#include <ctime>

Gameplay_screen::Gameplay_screen(GameEngine::Window* window) :m_window(window)
{
	m_index = SCREEN_INDEX_GAMEPLAY;
}

int Gameplay_screen::get_next_screen_index() const
{
	return GameEngine::NO_SCREEN_INDEX;
}

int Gameplay_screen::get_previous_screen_index() const
{
	return SCREEN_INDEX_MAINMENU;
}

void Gameplay_screen::build()
{
}

void Gameplay_screen::destroy()
{
}

void Gameplay_screen::on_entry()
{
	std::cout << "on_entry\n";

	b2Vec2 _gravity(0.0f, -25.0f);
	m_world = std::make_unique<b2World>(_gravity);

	m_debug_renderer.init();

	//Make the ground
	b2BodyDef _ground_body_def;
	_ground_body_def.position.Set(0.0f, -21.0f);
	b2Body* _ground_body = m_world->CreateBody(&_ground_body_def);

	//Make the ground fixture
	b2PolygonShape _ground_box;
	_ground_box.SetAsBox(50.0f, 10.0f);

	_ground_body->CreateFixture(&_ground_box, 0.0f);

	//Load the texture
	m_texture = GameEngine::ResourceManager::get_texture("Textures/bricks_top.png");

	//Make a bunch of boxes
	std::mt19937 _random_engine;
	std::uniform_real_distribution<float> _x_pos(-10.0f, 10.0f);
	std::uniform_real_distribution<float> _y_pos(-15.0f, 15.0f);
	std::uniform_real_distribution<float> _size(0.5f, 2.5f);
	std::uniform_int_distribution<int> _color(0, 255);

	int _num_boxes = 40;
	for (int i = 0; i < _num_boxes; i++)
	{
		GameEngine::ColorRGBA8 _rand_color;
		_rand_color.r = _color(_random_engine);
		_rand_color.g = _color(_random_engine);
		_rand_color.b = _color(_random_engine);
		_rand_color.a = 255;

		Box2D _new_box;
		_new_box.init(m_world.get(), glm::vec2(_x_pos(_random_engine), _y_pos(_random_engine)), glm::vec2(_size(_random_engine), _size(_random_engine)), m_texture, _rand_color);

		m_boxes2D.push_back(_new_box);
	}

	//Initialize m_sprite_batch
	m_sprite_batch.init();

	//Shader init
	//Compile my texture 
	m_texture_program.compile_shaders("Shaders/colorShading.vert", "Shaders/colorShading.frag");
	m_texture_program.add_attribute("vertex_position");
	m_texture_program.add_attribute("vertex_color");
	m_texture_program.add_attribute("vertex_UV_coordinates");
	m_texture_program.link_shaders();
	//Compile my light
	m_light_program.compile_shaders("Shaders/lightShading.vert", "Shaders/lightShading.frag");
	m_light_program.add_attribute("vertex_position");
	m_light_program.add_attribute("vertex_color");
	m_light_program.add_attribute("vertex_UV_coordinates");
	m_light_program.link_shaders();


	m_camera.init(m_window->get_screen_width(), m_window->get_screen_height());
	m_camera.set_scale(32.0f);

	//Init Player
	m_player.init(m_world.get(), glm::vec2(0.0f, 30.0f), glm::vec2(2.0f), glm::vec2(1.0f, 1.8f), GameEngine::ColorRGBA8(255, 255, 255, 255));

	init_UI();
}

void Gameplay_screen::on_exit()
{
	m_debug_renderer.dispose();
}

void Gameplay_screen::update()
{
	//std::cout << "update\n";
	m_camera.update();
	check_input();
	m_player.update(m_game->get_input_manager());
	//Update the physics simulation
	m_world->Step(1.0f/30.0f,6,2);
}

void Gameplay_screen::draw()
{
	//std::cout << "draw\n";
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	m_texture_program.use();

	//Upload texture uniform
	GLint _texture_uniform = m_texture_program.get_uniform_location("my_sampler");
	glUniform1i(_texture_uniform, 0);
	glActiveTexture(GL_TEXTURE0);

	//Camera matrix
	glm::mat4 _matrix_position = m_camera.get_camera_matrix();
	GLint _matrix_uniform = m_texture_program.get_uniform_location("matrix_position");
	glUniformMatrix4fv(_matrix_uniform, 1, GL_FALSE, &_matrix_position[0][0]);

	m_sprite_batch.begin();

	for (auto& box : m_boxes2D)
		box.draw(m_sprite_batch);

	m_player.draw(m_sprite_batch);

	m_sprite_batch.end();
	m_sprite_batch.render_batch();

	m_texture_program.unuse();

	//Debug rendering
	if (m_debug_renderable)
		draw_in_debug_mode(_matrix_position);

	//Render some test lights
	Light _player_light;
	_player_light.set_color(GameEngine::ColorRGBA8(255, 255, 255, 128));
	_player_light.set_position(m_player.get_position());
	_player_light.set_size(30.0f);

	Light _mouse_light;
	_mouse_light.set_color(GameEngine::ColorRGBA8(255, 0, 255, 128));
	glm::vec2 _mouse_position = m_camera.convert_screen_to_world(m_game->get_input_manager().get_mouse_coordinates());
	_mouse_light.set_position(_mouse_position);
	_mouse_light.set_size(30.0f);

	m_light_program.use();
	_matrix_uniform = m_texture_program.get_uniform_location("matrix_position");
	glUniformMatrix4fv(_matrix_uniform, 1, GL_FALSE, &_matrix_position[0][0]);

	//Additive blanding
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	m_sprite_batch.begin();

	_player_light.draw(m_sprite_batch);
	_mouse_light.draw(m_sprite_batch);

	m_sprite_batch.end();
	m_sprite_batch.render_batch();

	m_light_program.unuse();

	//Reset to regular alpha blending
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	m_gui->draw();
	glEnable(GL_BLEND);
}

void Gameplay_screen::init_UI()
{
	m_gui.reset(nullptr);
	m_gui = std::make_unique<GameEngine::GUI>("GUI");
	m_gui->load_scheme("TaharezLook.scheme");
	m_gui->load_font("DejaVuSans-10");

	CEGUI::PushButton*test_button = static_cast<CEGUI::PushButton*> (m_gui->create_widget("TaharezLook/Button", GameEngine::Widget_property(glm::vec4(0.5f, 0.5f, 0.1f, 0.05f), glm::vec4(0.0f)), "TestButton"));
	//test_button->setText("Hello World");
	test_button->setText("EXit Game");

	test_button->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Gameplay_screen::on_exit_clicked, this));

	CEGUI::Combobox*test_combobox = static_cast<CEGUI::Combobox*> (m_gui->create_widget("TaharezLook/Combobox", GameEngine::Widget_property(glm::vec4(0.2f, 0.2f, 0.1f, 0.05f), glm::vec4(0.0f)), "TestCombo"));

	//CEGUI::Combobox*test_combobox2 = static_cast<CEGUI::Combobox*> (m_gui->create_widget("TaharezLook/Combobox", GameEngine::Widget_property(glm::vec4(0.2f, 0.2f, 0.1f, 0.05f), glm::vec4(0.0f)), "TestCombo2"));

	m_gui->set_mouse_cursor("TaharezLook/MouseArrow");
	SDL_ShowCursor(0);
}

void Gameplay_screen::check_input()
{

	SDL_Event _evnt;
	while (SDL_PollEvent(&_evnt))
	{
		m_game->on_SDL_event(_evnt);
		m_gui->on_STD_event(_evnt);

		switch (_evnt.type)
		{
		case SDL_QUIT:
			on_exit_clicked(CEGUI::EventArgs());
			break;
		default:
			break;
		}
	}
}

bool Gameplay_screen::on_exit_clicked(const CEGUI::EventArgs& e)
{
	m_current_state = GameEngine::Screen_state::EXIT_APPLICATION;
	return true;
}

void Gameplay_screen::draw_in_debug_mode(glm::mat4& matrix_position)
{
	glm::vec4 _dest_rect;

	draw_debug_box(_dest_rect);
	draw_debug_player(_dest_rect);

	m_debug_renderer.end();
	m_debug_renderer.render(matrix_position, 2.0f);
}

void Gameplay_screen::draw_debug_box(glm::vec4 & dest_rect)
{
	for (auto& box : m_boxes2D)
	{
		dest_rect.x = box.get_body()->GetPosition().x - box.get_dimensions().x / 2.0f;
		dest_rect.y = box.get_body()->GetPosition().y - box.get_dimensions().y / 2.0f;
		dest_rect.z = box.get_dimensions().x;
		dest_rect.w = box.get_dimensions().y;

		m_debug_renderer.draw_box(dest_rect, GameEngine::ColorRGBA8(255, 255, 255, 255), box.get_body()->GetAngle());
		//m_debug_renderer.draw_circle(glm::vec2(box.get_body()->GetPosition().x, box.get_body()->GetPosition().y), GameEngine::ColorRGBA8(255, 255, 255, 255), box.get_dimensions().x/2.0f);
	}
}

void Gameplay_screen::draw_debug_player(glm::vec4 & dest_rect)
{
	m_player.draw_debug(m_debug_renderer);
}

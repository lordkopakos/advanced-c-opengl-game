#pragma once

#include "Box2D.h"
#include "Player.h"

#include<GameEngine\IScreen.h>
#include<GameEngine\SpriteBatch.h>
#include<GameEngine\GLSLProgram.h>
#include <GameEngine\Camera2D.h>
#include <GameEngine\GLTexture.h>
#include <GameEngine\Window.h>
#include <GameEngine\Debug_renderer.h>
#include <GameEngine\GUI.h>

#include<Box2D\Box2D.h>

#include<vector>

class Gameplay_screen : public GameEngine::IScreen
{
public:
	Gameplay_screen(GameEngine::Window* window);
	~Gameplay_screen() = default;

	virtual int get_next_screen_index() const override;
	virtual int get_previous_screen_index() const override;

	virtual void build() override;
	virtual void destroy() override;

	virtual void on_entry() override;
	virtual void on_exit() override;

	virtual void update() override;
	virtual void draw() override;

private:
	void init_UI();
	void check_input();

	bool on_exit_clicked(const CEGUI::EventArgs& e);

	//TODO: DebugRenderManager
	void draw_in_debug_mode(glm::mat4& matrix_position);
	void draw_debug_box(glm::vec4& dest_rect);
	void draw_debug_player(glm::vec4& dest_rect);

	GameEngine::SpriteBatch m_sprite_batch;
	GameEngine::GLSLProgram m_texture_program;
	GameEngine::GLSLProgram m_light_program;

	GameEngine::Camera2D m_camera;
	GameEngine::Window* m_window = nullptr;
	GameEngine::Debug_renderer m_debug_renderer;
	bool m_debug_renderable=false;

	std::unique_ptr<GameEngine::GUI> m_gui;

	Player m_player;

	GameEngine::GLTexture m_texture;

	std::unique_ptr<b2World> m_world = nullptr;
	std::vector<Box2D> m_boxes2D;
};


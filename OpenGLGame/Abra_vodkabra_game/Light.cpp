#include "Light.h"



Light::Light()
{
}


void Light::draw(GameEngine::SpriteBatch & sprite_batch)
{
	glm::vec4 _dest_rect;
	_dest_rect.x = m_position.x - m_size / 2.0f;
	_dest_rect.y = m_position.y - m_size / 2.0f;
	_dest_rect.z = m_size;
	_dest_rect.w = m_size;
	sprite_batch.draw(_dest_rect, glm::vec4(-1.0f,-1.0f,2.0f, 2.0f), 0, 0.0f, m_color, 0.0f);
}

#pragma once

#include<GameEngine\Vertex.h>
#include<GameEngine\SpriteBatch.h>
#include<glm\glm.hpp>
class Light
{
public:
	Light();
	~Light()=default;

	void draw(GameEngine::SpriteBatch& sprite_batch);

	void set_color(const GameEngine::ColorRGBA8& color) { m_color = color; }
	void set_position(const glm::vec2& position) { m_position = position; }
	void set_size(float size) { m_size = size; }
private:
	GameEngine::ColorRGBA8 m_color;
	glm::vec2 m_position;
	float m_size;
};


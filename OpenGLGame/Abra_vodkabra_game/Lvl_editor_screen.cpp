#include "Lvl_editor_screen.h"

#include<memory>

#include <glm\glm.hpp>

#include <GameEngine\ResourceManager.h>

Lvl_editor_screen::Lvl_editor_screen(GameEngine::Window * window) :m_window(window)
{
	m_index = SCREEN_INDEX_EDITOR;
}

int Lvl_editor_screen::get_next_screen_index() const
{
	return GameEngine::NO_SCREEN_INDEX;
}

int Lvl_editor_screen::get_previous_screen_index() const
{
	return SCREEN_INDEX_MAINMENU;
}

void Lvl_editor_screen::build()
{
}

void Lvl_editor_screen::destroy()
{
}

void Lvl_editor_screen::on_entry()
{
	m_camera.init(m_window->get_screen_width(), m_window->get_screen_height());
	m_camera.set_scale(1.0f);

	init_UI();
	m_sprite_batch.init();
	m_sprite_font = std::make_shared<GameEngine::SpriteFont>("Fonts/chintzy.ttf", 32);
	
	//Shader init
	//Compile my texture 
	m_texture_program.compile_shaders("Shaders/colorShading.vert", "Shaders/colorShading.frag");
	m_texture_program.add_attribute("vertex_position");
	m_texture_program.add_attribute("vertex_color");
	m_texture_program.add_attribute("vertex_UV_coordinates");
	m_texture_program.link_shaders();

	m_blank_texture=GameEngine::ResourceManager::get_texture("Textures/blank.png");
}

void Lvl_editor_screen::on_exit()
{
	m_texture_program.dispose();
}

void Lvl_editor_screen::update()
{
	m_camera.update();
	check_input();
}

void Lvl_editor_screen::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.2f, 1.0f);

	m_texture_program.use();

	//Upload texture uniform
	GLint _texture_uniform = m_texture_program.get_uniform_location("my_sampler");
	glUniform1i(_texture_uniform, 0);
	glActiveTexture(GL_TEXTURE0);

	//Camera matrix
	glm::mat4 _matrix_position = m_camera.get_camera_matrix();
	GLint _matrix_uniform = m_texture_program.get_uniform_location("matrix_position");
	glUniformMatrix4fv(_matrix_uniform, 1, GL_FALSE, &_matrix_position[0][0]);
	
	m_sprite_batch.begin();
	{//Draw the color picker quad
		constexpr float QUAD_SIZE = 90.0f;

		glm::vec4 _dest_rect;
		_dest_rect.x = m_b_slider->getXPosition().d_scale*m_window->get_screen_width() + 20.0f- m_window->get_screen_width()/2.0f + QUAD_SIZE / 4.0f;
		_dest_rect.y = m_window->get_screen_height() / 2.0f - m_b_slider->getYPosition().d_scale*m_window->get_screen_height() -m_b_slider->getHeight().d_scale * m_window->get_screen_height() *0.5f - QUAD_SIZE/2.0f;
		//_dest_rect.x = 0;
		//_dest_rect.y = 0;
		_dest_rect.z = QUAD_SIZE;
		_dest_rect.w = QUAD_SIZE;

		m_sprite_batch.draw(_dest_rect, 
			glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), m_blank_texture.id , 0.0f, GameEngine::ColorRGBA8((GLubyte)m_color_picker_red, (GLubyte)m_color_picker_green, (GLubyte)m_color_picker_blue, 255.0f));
	}

	{//Draw labels for rigid and dynamic buttons
		glm::vec2 pos;
		pos.x = m_rigid_radio_btn->getXPosition().d_scale * m_window->get_screen_width() - m_window->get_screen_width() / 2.0f + m_rigid_radio_btn->getWidth().d_offset / 2.0f;
		pos.y = m_window->get_screen_height() / 2.0f - m_rigid_radio_btn->getYPosition().d_scale * m_window->get_screen_height();
		m_sprite_font->draw(m_sprite_batch, "Rigid",pos, glm::vec2(0.7f),0.0f, GameEngine::ColorRGBA8(255.0f, 255.0f, 255.0f,255.0f), GameEngine::Justification::MIDDLE);

		pos.x = m_dynamic_radio_btn->getXPosition().d_scale * m_window->get_screen_width() - m_window->get_screen_width() / 2.0f + m_dynamic_radio_btn->getHeight().d_offset / 2.0f;
		pos.y = +m_window->get_screen_height() / 2.0f - m_dynamic_radio_btn->getYPosition().d_scale * m_window->get_screen_height();
		m_sprite_font->draw(m_sprite_batch, "Dynamic", pos, glm::vec2(0.7f), 0.0f, GameEngine::ColorRGBA8(255.0f, 255.0f, 255.0f, 255.0f), GameEngine::Justification::MIDDLE);
	}
	m_sprite_batch.end();

	m_sprite_batch.render_batch();
	m_texture_program.unuse();

	m_gui->draw();
	//glEnable(GL_BLEND);
}

void Lvl_editor_screen::init_UI()
{
	m_gui = std::make_unique<GameEngine::GUI>("GUI");
	m_gui->load_scheme("TaharezLook.scheme");
	m_gui->load_font("DejaVuSans-10");

	{//Add a color picker
		constexpr float X_DIM = 0.01f, Y_DIM = 0.1f;
		constexpr float Y_POS = 0.1f;
		constexpr float PADDING = 0.01f;
		float X_POS = 0.05f;

		 m_r_slider = static_cast<CEGUI::Slider*>(m_gui->create_widget(
			"TaharezLook/Slider", 
			GameEngine::Widget_property(glm::vec4(X_POS, Y_POS, X_DIM, Y_DIM), glm::vec4(0.0f)), 
			"RedSlider"));
		m_r_slider->setMaxValue(255.0f);
		m_r_slider->setCurrentValue(m_color_picker_red);
		m_r_slider->subscribeEvent(CEGUI::Slider::EventValueChanged, CEGUI::Event::Subscriber(&Lvl_editor_screen::on_color_picker_red_changed, this));
		m_r_slider->setClickStep(1.0f);

		m_g_slider = static_cast<CEGUI::Slider*>(m_gui->create_widget(
			"TaharezLook/Slider",
			GameEngine::Widget_property(glm::vec4(X_POS + X_DIM + PADDING, Y_POS, X_DIM, Y_DIM), glm::vec4(0.0f)),
			"GreenSlider"));
		m_g_slider->setMaxValue(255.0f);
		m_g_slider->setCurrentValue(m_color_picker_red);
		m_g_slider->subscribeEvent(CEGUI::Slider::EventValueChanged, CEGUI::Event::Subscriber(&Lvl_editor_screen::on_color_picker_green_changed, this));
		m_g_slider->setClickStep(1.0f);

		m_b_slider = static_cast<CEGUI::Slider*>(m_gui->create_widget(
			"TaharezLook/Slider",
			GameEngine::Widget_property(glm::vec4(X_POS + 2*(X_DIM+ PADDING), Y_POS, X_DIM, Y_DIM), glm::vec4(0.0f)),
			"BlueSlider"));
		m_b_slider->setMaxValue(255.0f);
		m_b_slider->setCurrentValue(m_color_picker_red);
		m_b_slider->subscribeEvent(CEGUI::Slider::EventValueChanged, CEGUI::Event::Subscriber(&Lvl_editor_screen::on_color_picker_blue_changed, this));
		m_b_slider->setClickStep(1.0f);
	}


	{//Add the physics mode radio buttons
		constexpr float Y_POS = 0.25f;
		constexpr float DIMS_PIXELS = 20.0f;
		constexpr float PADDING = 0.1f;
		m_rigid_radio_btn = static_cast<CEGUI::RadioButton*>(m_gui->create_widget("TaharezLook/RadioButton", GameEngine::Widget_property(glm::vec4(0.05f, Y_POS, 0.0f, 0.0f), glm::vec4(0.0f, 0.0f , DIMS_PIXELS, DIMS_PIXELS)), "RigidButton"));
		m_rigid_radio_btn->setSelected(true);
		m_rigid_radio_btn->subscribeEvent(CEGUI::RadioButton::EventMouseClick, CEGUI::Event::Subscriber(&Lvl_editor_screen::on_rigid_mouse_click, this));

		m_dynamic_radio_btn = static_cast<CEGUI::RadioButton*>(m_gui->create_widget("TaharezLook/RadioButton", GameEngine::Widget_property(glm::vec4(0.05f + PADDING, Y_POS, 0.0f, 0.0f), glm::vec4(0.0f, 0.0f, DIMS_PIXELS, DIMS_PIXELS)), "DynamicButton"));
		m_dynamic_radio_btn->setSelected(false);
		m_dynamic_radio_btn->subscribeEvent(CEGUI::RadioButton::EventMouseClick, CEGUI::Event::Subscriber(&Lvl_editor_screen::on_dynamic_mouse_click, this));
	}

	m_gui->set_mouse_cursor("TaharezLook/MouseArrow");
	SDL_ShowCursor(0);
}

void Lvl_editor_screen::check_input()
{
	SDL_Event _evnt;
	while (SDL_PollEvent(&_evnt))
	{
		m_gui->on_STD_event(_evnt);
		switch (_evnt.type)
		{
		case SDL_QUIT:
			on_exit_clicked(CEGUI::EventArgs());
			break;
		default:
			break;
		}
	}
}

bool Lvl_editor_screen::on_exit_clicked(const CEGUI::EventArgs & e)
{
	m_current_state = GameEngine::Screen_state::EXIT_APPLICATION;
	return true;
}

bool Lvl_editor_screen::on_color_picker_red_changed(const CEGUI::EventArgs & e)
{
	m_color_picker_red = m_r_slider->getCurrentValue();
	return true;
}

bool Lvl_editor_screen::on_color_picker_green_changed(const CEGUI::EventArgs & e)
{
	m_color_picker_green = m_g_slider->getCurrentValue();
	return true;
}

bool Lvl_editor_screen::on_color_picker_blue_changed(const CEGUI::EventArgs & e)
{
	m_color_picker_blue = m_b_slider->getCurrentValue();
	return true;
}

bool Lvl_editor_screen::on_rigid_mouse_click(const CEGUI::EventArgs & e)
{
	m_physicsMode = PhysicsMode::RIGID;
	return true;
}

bool Lvl_editor_screen::on_dynamic_mouse_click(const CEGUI::EventArgs & e)
{
	m_physicsMode = PhysicsMode::DYNAMIC;
	return true;
}

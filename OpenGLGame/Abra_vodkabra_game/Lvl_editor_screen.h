#pragma once
#include <GameEngine\IScreen.h>
#include <GameEngine\Window.h>
#include <GameEngine\Camera2D.h>
#include <GameEngine\GUI.h>
#include <GameEngine\SpriteBatch.h>
#include <GameEngine\GLSLProgram.h>
#include <GameEngine\SpriteFont.h>
#include <GameEngine\GLTexture.h>
#include "Screen_indices.h"

#include <memory>

enum class PhysicsMode
{
	RIGID,
	DYNAMIC
};

class Lvl_editor_screen : public GameEngine::IScreen
{
public:
	Lvl_editor_screen(GameEngine::Window* window);
	~Lvl_editor_screen() = default;

	virtual int get_next_screen_index() const override;
	virtual int get_previous_screen_index() const override;

	virtual void build() override;
	virtual void destroy() override;

	virtual void on_entry() override;
	virtual void on_exit() override;

	virtual void update() override;
	virtual void draw() override;

private:
	void init_UI();
	void check_input();

	bool on_exit_clicked(const CEGUI::EventArgs& e);

	bool on_color_picker_red_changed(const CEGUI::EventArgs& e);
	bool on_color_picker_green_changed(const CEGUI::EventArgs& e);
	bool on_color_picker_blue_changed(const CEGUI::EventArgs& e);

	bool on_rigid_mouse_click(const CEGUI::EventArgs& e);
	bool on_dynamic_mouse_click(const CEGUI::EventArgs& e);

	float m_color_picker_red = 255.0f;
	float m_color_picker_green = 255.0f;
	float m_color_picker_blue = 255.0f;

	CEGUI::Slider* m_r_slider = nullptr;
	CEGUI::Slider* m_g_slider = nullptr;
	CEGUI::Slider* m_b_slider = nullptr;
	CEGUI::RadioButton* m_rigid_radio_btn = nullptr;
	CEGUI::RadioButton* m_dynamic_radio_btn = nullptr;

	GameEngine::SpriteBatch m_sprite_batch;
	std::shared_ptr<GameEngine::SpriteFont> m_sprite_font = nullptr;
	GameEngine::GLSLProgram m_texture_program;

	GameEngine::GLTexture m_blank_texture = {0,0,0};

	PhysicsMode m_physicsMode = PhysicsMode::RIGID;

	GameEngine::Camera2D m_camera;
	GameEngine::Window* m_window = nullptr;
	std::unique_ptr<GameEngine::GUI> m_gui;
};
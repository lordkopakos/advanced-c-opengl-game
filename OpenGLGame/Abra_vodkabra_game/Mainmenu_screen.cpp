#include "Mainmenu_screen.h"

Mainmenu_screen::Mainmenu_screen(GameEngine::Window * window) : m_window(window)
{
	m_index = SCREEN_INDEX_MAINMENU;
}

int Mainmenu_screen::get_next_screen_index() const
{
	return m_next_screen_index;
}

int Mainmenu_screen::get_previous_screen_index() const
{
	return GameEngine::NO_SCREEN_INDEX;
}

void Mainmenu_screen::build()
{
}

void Mainmenu_screen::destroy()
{
}

void Mainmenu_screen::on_entry()
{
	m_camera.init(m_window->get_screen_width(), m_window->get_screen_height());
	m_camera.set_scale(32.0f);

	init_UI();
}

void Mainmenu_screen::on_exit()
{
}

void Mainmenu_screen::update()
{
	m_camera.update();
	check_input();
}

void Mainmenu_screen::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.2f, 1.0f);

	m_gui->draw();
	glEnable(GL_BLEND);
}

void Mainmenu_screen::init_UI()
{
	m_gui = std::make_unique<GameEngine::GUI>("GUI");
	m_gui->load_scheme("TaharezLook.scheme");
	m_gui->load_font("DejaVuSans-10");

	CEGUI::PushButton*_new_game_button = static_cast<CEGUI::PushButton*> (m_gui->create_widget("TaharezLook/Button", GameEngine::Widget_property(glm::vec4(0.45f, 0.5f, 0.1f, 0.05f), glm::vec4(0.0f)), "NewGameButton"));
	_new_game_button->setText("New Game");

	_new_game_button->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Mainmenu_screen::on_new_game_clicked, this));

	CEGUI::PushButton*_editor_button = static_cast<CEGUI::PushButton*> (m_gui->create_widget("TaharezLook/Button", GameEngine::Widget_property(glm::vec4(0.45f, 0.56f, 0.1f, 0.05f), glm::vec4(0.0f)), "EditorButton"));
	_editor_button->setText("Map Editor");

	_editor_button->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Mainmenu_screen::on_editor_clicked, this));

	CEGUI::PushButton*_exit_button = static_cast<CEGUI::PushButton*> (m_gui->create_widget("TaharezLook/Button", GameEngine::Widget_property(glm::vec4(0.45f, 0.62f, 0.1f, 0.05f), glm::vec4(0.0f)), "ExitButton"));
	_exit_button->setText("Exit Game");

	_exit_button->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Mainmenu_screen::on_exit_clicked, this));

	m_gui->set_mouse_cursor("TaharezLook/MouseArrow");
	SDL_ShowCursor(0);
}

void Mainmenu_screen::check_input()
{
	SDL_Event _evnt;
	while (SDL_PollEvent(&_evnt))
	{
		m_gui->on_STD_event(_evnt);
		switch (_evnt.type)
		{
		case SDL_QUIT:
			on_exit_clicked(CEGUI::EventArgs());
			break;
		default:
			break;
		}
	}
}

bool Mainmenu_screen::on_new_game_clicked(const CEGUI::EventArgs & e)
{
	m_next_screen_index = SCREEN_INDEX_GAMEPLAY;
	m_current_state = GameEngine::Screen_state::CHANGE_NEXT;
	return true;
}

bool Mainmenu_screen::on_editor_clicked(const CEGUI::EventArgs & e)
{
	m_next_screen_index = SCREEN_INDEX_EDITOR;
	m_current_state = GameEngine::Screen_state::CHANGE_NEXT;
	return true;
}

bool Mainmenu_screen::on_exit_clicked(const CEGUI::EventArgs & e)
{
	m_current_state = GameEngine::Screen_state::EXIT_APPLICATION;
	return true;
}

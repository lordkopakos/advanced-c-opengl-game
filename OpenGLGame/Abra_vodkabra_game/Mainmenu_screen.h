#pragma once
#include <GameEngine\IScreen.h>
#include <GameEngine\Window.h>
#include <GameEngine\Camera2D.h>
#include <GameEngine\GUI.h>

#include "Screen_indices.h"

class Mainmenu_screen : public GameEngine::IScreen
{
public:
	Mainmenu_screen(GameEngine::Window* window);
	~Mainmenu_screen() = default;

	virtual int get_next_screen_index() const override;
	virtual int get_previous_screen_index() const override;

	virtual void build() override;
	virtual void destroy() override;

	virtual void on_entry() override;
	virtual void on_exit() override;

	virtual void update() override;
	virtual void draw() override;

private:
	void init_UI();
	void check_input();

	bool on_new_game_clicked(const CEGUI::EventArgs& e);
	bool on_editor_clicked(const CEGUI::EventArgs& e);
	bool on_exit_clicked(const CEGUI::EventArgs& e);

	int m_next_screen_index = SCREEN_INDEX_GAMEPLAY;

	GameEngine::Camera2D m_camera;
	GameEngine::Window* m_window = nullptr;
	std::unique_ptr<GameEngine::GUI> m_gui;
};
#include "Player.h"
#include <GameEngine\ResourceManager.h>

#include <SDL\SDL.h>

void Player::init(b2World * world, const glm::vec2 & position, const glm::vec2& draw_dimensions,
	const glm::vec2 & collision_dimensions, const GameEngine::ColorRGBA8 & color)
{
	//Load the texture
	GameEngine::GLTexture _texture = GameEngine::ResourceManager::get_texture("Textures/blue_ninja.png");
	m_color = color;
	m_draw_dimensions = draw_dimensions;

	m_collision_capsule.init(world, position, collision_dimensions,1.0f,0.1f, true);
	m_tile_sheet.init(_texture, glm::ivec2(10, 2));
}

void Player::draw(GameEngine::SpriteBatch & sprite_batch)
{
	glm::vec4 _dest_rect;
	//std::unique_ptr<b2Body> _body = std::unique_ptr<b2Body>(m_collision_capsule.get_body());
	b2Body* _body = m_collision_capsule.get_body();

	_dest_rect.x = _body->GetPosition().x - m_draw_dimensions.x / 2.0f;
	_dest_rect.y = _body->GetPosition().y - m_collision_capsule.get_dimensions().y / 2.0f;
	_dest_rect.z = m_draw_dimensions.x;
	_dest_rect.w = m_draw_dimensions.y;

	int _tile_index;
	int _num_tiles;
	float _animation_speed=0.4f;
	
	glm::vec2 _velocity;
	_velocity.x = _body->GetLinearVelocity().x;
	_velocity.y = _body->GetLinearVelocity().y;

	if (m_on_ground)
	{
		if (m_punching)
		{
			_num_tiles = 4;
			_tile_index = 1;
			if (m_move_state != Player_move_state::PUNCHING)
			{
				m_move_state = Player_move_state::PUNCHING;
				m_animation_time = 0.0f;
			}
		}
		else if (abs(_velocity.x) > 1.0f && ((_velocity.x>0 && m_direction>0) || (_velocity.x<0 && m_direction<0)))
		{
			//Running
			_num_tiles = 6;
			_tile_index = 10;
			_animation_speed = abs(_velocity.x)*0.06f;
			if (m_move_state != Player_move_state::RUNNING)
			{
				m_move_state = Player_move_state::RUNNING;
				m_animation_time = 0.0f;
			}
		}
		else
		{
			//Standing still
			_num_tiles = 1;
			_tile_index = 0;
			m_move_state = Player_move_state::STANDING;
		}
	}
	else 
	{
		//In the air

		if (m_punching)
		{
			_num_tiles = 1;
			_tile_index = 18;
			_animation_speed *= 0.25f;
			if (m_move_state != Player_move_state::PUNCHING)
			{
				m_move_state = Player_move_state::PUNCHING;
				m_animation_time = 0.0f;
			}
		}
		else if (abs(_velocity.x) > 5.0f)
		{
			_num_tiles = 1;
			_tile_index = 10;
			m_move_state = Player_move_state::IN_AIR;
		}
		else if (_velocity.y <= 0.0f)
		{
			//Falling
			_num_tiles = 1;
			_tile_index = 17;
			m_move_state = Player_move_state::IN_AIR;
		}
		else
		{
			//Rising
			_num_tiles = 1;
			_tile_index = 16;
			m_move_state = Player_move_state::IN_AIR;
		}
	}

	//Increment animation time
	m_animation_time += _animation_speed;

	//Check for punch
	if (m_animation_time > _num_tiles)
	{
		m_punching = false;
	}

	//Apply animation
	_tile_index = _tile_index + static_cast<int>(m_animation_time) % _num_tiles;

	glm::vec4 _UV_rect = m_tile_sheet.get_UVs(_tile_index);
	//Check direction
	if (m_direction == -1)
	{
		_UV_rect.x += 1.0f / m_tile_sheet.get_dimensions().x;
		_UV_rect.z *= -1;
	}

	sprite_batch.draw(_dest_rect, _UV_rect, m_tile_sheet.get_texture().id, 0.0f, m_color, _body->GetAngle());
}

void Player::draw_debug(GameEngine::Debug_renderer & debug_renderer)
{
	m_collision_capsule.draw_debug(debug_renderer);
}

void Player::update(GameEngine::Input_Manager& input_manager)
{
	b2Body* _body = m_collision_capsule.get_body();

	if (input_manager.is_key_down(SDLK_a))
	{
		_body->ApplyForceToCenter(b2Vec2(-100.0, 0.0), true);
		m_direction = -1;
	}
	else if (input_manager.is_key_down(SDLK_d))
	{
		_body->ApplyForceToCenter(b2Vec2(100.0, 0.0), true);
		m_direction = 1;
	}
	else
		_body->SetLinearVelocity(b2Vec2(_body->GetLinearVelocity().x*0.95, _body->GetLinearVelocity().y));

	if (input_manager.is_key_pressed(SDLK_SPACE))
		m_punching = true;

	constexpr float MAX_SPEED = 6.0f;

	if(_body->GetLinearVelocity().x< -MAX_SPEED)
		_body->SetLinearVelocity(b2Vec2(-MAX_SPEED, _body->GetLinearVelocity().y));
	else if (_body->GetLinearVelocity().x > MAX_SPEED)
		_body->SetLinearVelocity(b2Vec2(MAX_SPEED, _body->GetLinearVelocity().y));

	m_on_ground = false;

	//Loop through all the contact points
	for (b2ContactEdge* _ce = _body->GetContactList(); _ce != nullptr; _ce = _ce->next)
	{
		b2Contact* _contact=_ce->contact;
		if (_contact->IsTouching())
		{
			b2WorldManifold _manifold;
			_contact->GetWorldManifold(&_manifold);

			//Check if the points are below
			//bool below = false;
			for (int _index = 0; _index < b2_maxManifoldPoints; _index++)
				if (_manifold.points[_index].y < _body->GetPosition().y - m_collision_capsule.get_dimensions().y / 2.0f+ m_collision_capsule.get_dimensions().x / 2.0f + 0.01f)
				{
					//below = true;
					break;
				}

			//if (below) //We can jump
			m_on_ground = true;
				if (input_manager.is_key_pressed(SDLK_w))
				{
					_body->ApplyLinearImpulse(b2Vec2(0.0f, 30.0f), b2Vec2(0.0f, 0.0f), true);
					break;
				}
		}
	}
}

#pragma once
#include "Capsule2D.h"

#include <GameEngine\SpriteBatch.h>
#include <GameEngine\GLTexture.h>
#include <GameEngine\Input_Manager.h>
#include <GameEngine\Tile_sheet.h>

enum class Player_move_state {STANDING, RUNNING, PUNCHING, IN_AIR};

class Player {
public:

	void init(b2World* world, 
		const glm::vec2& position, 
		const glm::vec2& draw_dimensions, 
		const glm::vec2& collision_dimensions, 
		const GameEngine::ColorRGBA8& color);
	
	void draw(GameEngine::SpriteBatch& sprite_batch);
	void draw_debug(GameEngine::Debug_renderer& debug_renderer);

	void update(GameEngine::Input_Manager& input_manager);

	const Capsule2D& get_capsule() const { return m_collision_capsule; };

	glm::vec2 get_position() const { 
		glm::vec2 _r_pos;
		_r_pos.x= m_collision_capsule.get_body()->GetPosition().x;
		_r_pos.y = m_collision_capsule.get_body()->GetPosition().y;
		return _r_pos;
	}
private:
	glm::vec2 m_draw_dimensions;
	GameEngine::ColorRGBA8 m_color;
	GameEngine::Tile_sheet m_tile_sheet;
	Capsule2D m_collision_capsule;

	short m_direction=1; //1 or -1
	//State animation and animation
	Player_move_state m_move_state = Player_move_state::STANDING;
	float m_animation_time = 0.0f;
	bool m_on_ground = false;
	bool m_punching = false;
};
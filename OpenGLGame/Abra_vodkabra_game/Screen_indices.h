#pragma once

constexpr int SCREEN_INDEX_MAINMENU = 0;
constexpr int SCREEN_INDEX_GAMEPLAY = 1;
constexpr int SCREEN_INDEX_EDITOR = 2;
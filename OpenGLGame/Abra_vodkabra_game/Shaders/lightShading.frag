#version 130

in vec2 fragment_position;

in vec4 fragment_color;

in vec2 fragment_UV_coordinates;

out vec4 color;

void main(){
	float distance =length(fragment_UV_coordinates);
	color = vec4(fragment_color.rgb, fragment_color.a*(pow(0.01,distance) - 0.01) );
}
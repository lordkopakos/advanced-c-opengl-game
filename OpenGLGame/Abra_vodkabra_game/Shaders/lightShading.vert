#version 130

//input data from the VBO. Each vertex is 2 floats
in vec2 vertex_position;
in vec4 vertex_color;
in vec2 vertex_UV_coordinates;

//if I write "flat out vec4 fragment_color"&"flat in vec4 fragment_color"<-frag, it will be without color interpolation 
out vec4 fragment_color;
out vec2 fragment_position;
out vec2 fragment_UV_coordinates;

uniform mat4 matrix_position;

void main(){
	//Set the x, y position on the screen
	gl_Position.xy = (matrix_position * vec4(vertex_position, 0.0, 1.0)).xy;
	//The z position is zero since I'm in 2D  
	gl_Position.z = 0.0;

	//Indicate that the coordinates are normalized
	gl_Position.w = 1.0;

	fragment_position=vertex_position;

	fragment_color=vertex_color;

	fragment_UV_coordinates=vertex_UV_coordinates;
}
#include "AudioEngine.h"
#include"Errors.h"
namespace GameEngine {

	void Sound_effect::play(int loops/*=0*/)
	{
		if(Mix_PlayChannel(-1, m_chunk.get(), loops)==-1)
			if (Mix_PlayChannel(0, m_chunk.get(), loops) == -1)
				fatal_error("Mix_PlayChannel error" + std::string(Mix_GetError()));
	}
	void Music::play(int loops/*=1*/)
	{
		Mix_PlayMusic(m_music.get(), loops);
	}

	void Music::pause()
	{
		Mix_PauseMusic();
	}

	void Music::stop()
	{
		Mix_HaltMusic();
	}

	void Music::resume()
	{
		Mix_ResumeMusic();
	}

	AudioEngine::AudioEngine()
	{
	}


	AudioEngine::~AudioEngine()
	{
		destroy();
	}
	void AudioEngine::init()
	{
		if (is_initialized())
			fatal_error("Tried to initialize Audio Engine twice!\n");

		// Parametr can be a bitwise combination of MIX_INIT_FAC, MIX_INIT_MOD, MIX_INIT_MP3, MIX_INIT_ORGG
		if (Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG) == -1)
			fatal_error("Mix_Init error" +std::string(Mix_GetError()));

		if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024)==-1)
			fatal_error("Mix_OpenAudio error" + std::string(Mix_GetError()));

		m_initializable = true;
	}
	void AudioEngine::destroy()
	{
		if (is_initialized())
		{
			m_initializable = false;

			m_effect_map.clear();
			m_music_map.clear();

			Mix_CloseAudio();
			Mix_Quit();
		}
	}
	Sound_effect AudioEngine::load_sound_effect(const std::string & file_path)
	{
		auto _effect_itr = m_effect_map.find(file_path);

		Sound_effect _effect;

		if (_effect_itr == m_effect_map.end())
		{
			std::shared_ptr<Mix_Chunk> _chunk = std::shared_ptr<Mix_Chunk>(Mix_LoadWAV(file_path.c_str()));
			if(_chunk==nullptr)
				fatal_error("Mix_LoadWAV error" + std::string(Mix_GetError()));
			
			_effect.m_chunk = _chunk;
			m_effect_map.insert({ file_path, _chunk });
		}
		else
			_effect.m_chunk = _effect_itr->second;

		return _effect;
	}
	Music AudioEngine::load_music(const std::string file_path)
	{
		auto _music_itr = m_music_map.find(file_path);

		Music _music;

		if (_music_itr == m_music_map.end())
		{
			std::shared_ptr<Mix_Music> _mix_music = std::shared_ptr<Mix_Music>(Mix_LoadMUS(file_path.c_str()));
			if (_mix_music == nullptr)
				fatal_error("Mix_LoadMUS error" + std::string(Mix_GetError()));

			_music.m_music = _mix_music;
			m_music_map.insert({ file_path, _mix_music });
		}
		else
			_music.m_music = _music_itr->second;

		return _music;
	}
	bool AudioEngine::is_initialized()
	{
		return m_initializable;
	}

}
#pragma once
#include<SDL\SDL_mixer.h>
#include<memory>
#include<string>
#include<map>
namespace GameEngine {

	class Sound_effect {
	public:
		friend class AudioEngine;

		//Plays the audio file
		//@param loops: If loops == -1 LOOP FOREVER,
		//otherwise play it LOOPS+1 TIMES
		void play(int loops = 0);
	private:
		std::shared_ptr<Mix_Chunk> m_chunk = nullptr;
	};

	class Music {
	public: 
		friend class AudioEngine;

		//Plays the audio file
		//@param loops: If loops == -1 LOOP FOREVER,
		//otherwise play it LOOPS TIMES
		void play(int loops = 1);
		void pause();
		void stop();
		void resume();
	private:
		std::shared_ptr<Mix_Music> m_music = nullptr;
	};

	class AudioEngine
	{
	public:
		AudioEngine();
		~AudioEngine();

		void init();
		void destroy();

		Sound_effect load_sound_effect(const std::string& file_path);
		Music load_music(const std::string file_path);
		
	private:
		bool is_initialized();

		std::map<std::string, std::shared_ptr<Mix_Chunk>> m_effect_map;
		std::map<std::string, std::shared_ptr<Mix_Music>> m_music_map;

		bool m_initializable;
	};

}
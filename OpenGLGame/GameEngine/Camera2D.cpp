#include "Camera2D.h"


namespace GameEngine {
	Camera2D::Camera2D() 
		:m_position(0, 0), 
		m_camera_matrix(1.0f), 
		m_scale(1.0f), 
		m_needs_matrix_update(1), 
		m_screen_width(500),
		m_screen_height(500),
		m_ortho_matrix(1.0f)
	{
	}


	Camera2D::~Camera2D()
	{
	}

	void Camera2D::init(int screen_width, int screen_height)
	{
		m_screen_width = screen_width;
		m_screen_height = screen_height;
		m_ortho_matrix = glm::ortho(0.0f, (float)m_screen_width, 0.0f, (float)m_screen_height);
	}

	void Camera2D::update()
	{
		if (m_needs_matrix_update) {

			//Camera translation
			glm::vec3 _translate(-m_position.x+m_screen_width / 2, -m_position.y+ m_screen_height / 2, 0.0f);
			m_camera_matrix = glm::translate(m_ortho_matrix, _translate);

			//Scale translation
			glm::vec3 _scale(m_scale, m_scale, 0.0f);
			m_camera_matrix = glm::scale(glm::mat4(1.0f), _scale)*m_camera_matrix;

			m_needs_matrix_update = false;
		}
	}

	glm::vec2 Camera2D::convert_screen_to_world(glm::vec2 screen_coordinates)
	{
		//Invert Y direction 
		screen_coordinates.y = m_screen_height - screen_coordinates.y;

		//Make it so that 0 is the center
		screen_coordinates -= glm::vec2(m_screen_width / 2, m_screen_height / 2);
		screen_coordinates /= m_scale;

		//Translate with the camera position
		return screen_coordinates += get_position();
	}

	bool Camera2D::is_box_in_view(const glm::vec2 & position, const glm::vec2 & dimensions)
	{
		glm::vec2 scaled_screen_dimensions = glm::vec2(m_screen_width, m_screen_height) / (m_scale);

		const float MIN_DISTANCE_X = dimensions.x/2.0f + scaled_screen_dimensions.x / 2.0f;
		const float MIN_DISTANCE_Y = dimensions.y/2.0f + scaled_screen_dimensions.y / 2.0f;

		glm::vec2 _center_position = position + dimensions/2.0f;
		glm::vec2 _center_camera_position = m_position;

		glm::vec2 _distance_vec = _center_position - _center_camera_position;

		float _depth_x = MIN_DISTANCE_X - abs(_distance_vec.x);
		float _depth_y = MIN_DISTANCE_Y - abs(_distance_vec.y);

		//If is true we are colliding
		if (_depth_x > 0 && _depth_y > 0)
			return true;
		return false;
	}
}

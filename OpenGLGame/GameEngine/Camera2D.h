#pragma once
#include<glm\glm.hpp>
#include<glm\gtc\matrix_transform.hpp>
namespace GameEngine {
	class Camera2D
	{
	public:
		Camera2D();
		~Camera2D();

		void init(int screen_width, int screen_height);
		void update();

		glm::vec2 convert_screen_to_world(glm::vec2 screen_coordinates);

		bool is_box_in_view(const glm::vec2& position, const glm::vec2& dimensions);

		//setters
		void set_position(glm::vec2& new_position) { m_position = new_position; m_needs_matrix_update = true; }
		void set_scale(float new_scale) { m_scale = new_scale; m_needs_matrix_update = true; }
		//getters
		glm::vec2 get_position() { return m_position; }
		float get_scale() { return m_scale; }
		glm::mat4 get_camera_matrix() { return m_camera_matrix; }

	private:
		int m_screen_width, m_screen_height;
		bool m_needs_matrix_update;
		float m_scale;
		glm::vec2 m_position;
		glm::mat4 m_camera_matrix;
		glm::mat4 m_ortho_matrix;
	};
}
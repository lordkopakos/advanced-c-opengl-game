#include "Debug_renderer.h"
#include "Utilities.h"

constexpr float PI = 3.14159265359f;
constexpr int NUM_VERTS = 100;

namespace Shader_source {
		constexpr char* VERT_SRC =
			R"(#version 130
	//The vertex shader operates on each vertex
	
	//input data from the VBO. Each vertex is 2 floats
	in vec2 vertex_position;
	in vec4 vertex_color;
	
	out vec2 fragment_position;
	out vec4 fragment_color;
	
	uniform mat4 matrix_position;
	
	void main() {
	    //Set the x,y position on the screen
	    gl_Position.xy = (matrix_position * vec4(vertex_position, 0.0, 1.0)).xy;
	    //the z position is zero since we are in 2D
	    gl_Position.z = 0.0;
	    
	    //Indicate that the coordinates are normalized
	    gl_Position.w = 1.0;
	    
	    fragment_position = vertex_position;
	    
	    fragment_color = vertex_color;
	})";
	
		constexpr char* FRAG_SRC = R"(#version 130
	//The fragment shader operates on each pixel in a given polygon
	
	in vec2 fragment_position;
	in vec4 fragment_color;
	
	//This is the 3 component float vector that gets outputted to the screen
	//for each pixel.
	out vec4 color;
	
	void main() {
	
	    color = fragment_color;
	})";
	}

namespace GameEngine {

	Debug_renderer::Debug_renderer() {
		// Empty
	}

	Debug_renderer::~Debug_renderer() {
		dispose();
	}

	void Debug_renderer::init() {

		// Shader init
		m_program.compile_shaders_from_source(Shader_source::VERT_SRC, Shader_source::FRAG_SRC);
		m_program.add_attribute("vertex_position");
		m_program.add_attribute("vertex_color");
		m_program.link_shaders();

		// Set up buffers
		glGenVertexArrays(1, &m_vao);
		glGenBuffers(1, &m_vbo);
		glGenBuffers(1, &m_ibo);

		glBindVertexArray(m_vao);
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(DebugVertex), (void*)offsetof(DebugVertex, position));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(DebugVertex), (void*)offsetof(DebugVertex, color));

		glBindVertexArray(0);
	}

	void Debug_renderer::end() {
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		// Orphan the buffer
		glBufferData(GL_ARRAY_BUFFER, m_verts.size() * sizeof(DebugVertex), nullptr, GL_DYNAMIC_DRAW);
		// Upload the data
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_verts.size() * sizeof(DebugVertex), m_verts.data());
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
		// Orphan the buffer
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(GLuint), nullptr, GL_DYNAMIC_DRAW);
		// Upload the data
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, m_indices.size() * sizeof(GLuint), m_indices.data());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		m_numElements = m_indices.size();
		m_indices.clear();
		m_verts.clear();
	}

	void Debug_renderer::render(const glm::mat4& projectionMatrix, float lineWidth) {
		m_program.use();

		GLint pUniform = m_program.get_uniform_location("matrix_position");
		glUniformMatrix4fv(pUniform, 1, GL_FALSE, &projectionMatrix[0][0]);

		glLineWidth(lineWidth);
		glBindVertexArray(m_vao);
		glDrawElements(GL_LINES, m_numElements, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);

		m_program.unuse();
	}

	void Debug_renderer::dispose() {
		if (m_vao) {
			glDeleteVertexArrays(1, &m_vao);
		}
		if (m_vbo) {
			glDeleteBuffers(1, &m_vbo);
		}
		if (m_ibo) {
			glDeleteBuffers(1, &m_ibo);
		}
		m_program.dispose();
	}

	void Debug_renderer::draw_box(const glm::vec4& dest_rect, const GameEngine::ColorRGBA8& color, float angle) {

		int _vsize = m_verts.size();
		m_verts.resize(m_verts.size() + 4);

		init_box_debug_vertices(_vsize, dest_rect, color, angle);
		init_box_indices(_vsize);
	}

	void Debug_renderer::init_box_debug_vertices(int vsize, const glm::vec4 & dest_rect, const GameEngine::ColorRGBA8 & color, float angle)
	{
		glm::vec2 _half_dims{ dest_rect.z / 2.0f, dest_rect.w / 2.0f };
	
		//Get points centered at origin
		glm::vec2 _tl{ -_half_dims.x, _half_dims.y };
		glm::vec2 _bl{ -_half_dims.x, -_half_dims.y };
		glm::vec2 _br{ _half_dims.x, -_half_dims.y };
		glm::vec2 _tr{ _half_dims.x, _half_dims.y };
	
		glm::vec2 _position_offset(dest_rect.x, dest_rect.y);
	
		//Rotate the points minus because is left, not right 
		m_verts[vsize].position = rotate_point(_tl, angle) + _half_dims + _position_offset;
		m_verts[vsize + 1].position = rotate_point(_bl, angle) + _half_dims + _position_offset;
		m_verts[vsize + 2].position = rotate_point(_br, angle) + _half_dims + _position_offset;
		m_verts[vsize + 3].position = rotate_point(_tr, angle) + _half_dims + _position_offset;
	
		for (int _index = vsize; _index < vsize + 4; _index++)
			m_verts[_index].color = color;
	}
	
	void Debug_renderer::init_box_indices(int vsize)
	{
		m_indices.reserve(m_indices.size() + 8);
		m_indices.emplace_back(vsize);
		for (int index = vsize + 1; index <= vsize + 3; index++)
			add_same_indices(index, 2);
		m_indices.emplace_back(vsize);
	}
	
	void Debug_renderer::add_same_indices(int index, short number_of_indices)
	{
		for (; number_of_indices > 0; number_of_indices--)
			m_indices.emplace_back(index);
	}

	void Debug_renderer::draw_circle(const glm::vec2 & center, const GameEngine::ColorRGBA8 & color, float radius)
	{
		auto _start = m_verts.size();
		init_circle_vertices(_start,center, color, radius);
		init_circle_indices(_start);
	}
	
	void Debug_renderer::init_circle_vertices(int start, const glm::vec2 & center, const GameEngine::ColorRGBA8 & color, float radius)
	{
		m_verts.resize(m_verts.size() + NUM_VERTS);

		for (auto _vertex_index = 0; _vertex_index < NUM_VERTS; _vertex_index++)
		{
			float _angle = (static_cast<float>(_vertex_index) / NUM_VERTS)* PI * 2.0f;
			m_verts[start + _vertex_index].position.x = cos(_angle)*radius+ center.x;
			m_verts[start + _vertex_index].position.y = sin(_angle)*radius + center.y;
			m_verts[start + _vertex_index].color = color;
		}
	}
	
	void Debug_renderer::init_circle_indices(int start)
	{

		m_indices.reserve(m_indices.size() + NUM_VERTS * 2);
		for (int _index = 0; _index < NUM_VERTS - 1; _index++)
		{
			m_indices.emplace_back(start + _index);
			m_indices.emplace_back(start + _index + 1);
		}
		m_indices.emplace_back(start + NUM_VERTS - 1);
		m_indices.emplace_back(start);
	}

}
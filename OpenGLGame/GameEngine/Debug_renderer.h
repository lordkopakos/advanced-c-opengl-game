#pragma once

#include"GLSLProgram.h"
#include "Vertex.h"

#include<glm\glm.hpp>

#include<vector>

namespace GameEngine {
	class Debug_renderer {
	public:
		Debug_renderer();
		~Debug_renderer();

		void init();
		void end();
		void draw_box(const glm::vec4& dest_rect, const GameEngine::ColorRGBA8& color, float angle);
		void draw_circle(const glm::vec2& center, const GameEngine::ColorRGBA8& color, float radius);
		void render(const glm::mat4& projectionMatrix, float lineWidth);
		void dispose();

		struct DebugVertex {
			glm::vec2 position;
			GameEngine::ColorRGBA8 color;
		};
	private:
		void init_box_debug_vertices(int vsize,const glm::vec4 & dest_rect, const GameEngine::ColorRGBA8 & color, float angle);
		void init_box_indices(int vsize);
		void add_same_indices(int index, short number_of_indices);
		
		void init_circle_vertices(int start, const glm::vec2& center, const GameEngine::ColorRGBA8& color, float radius);
		void init_circle_indices(int start);
		GameEngine::GLSLProgram m_program;
		std::vector<DebugVertex> m_verts;
		std::vector<GLuint> m_indices;
		GLuint m_vbo = 0, m_vao = 0, m_ibo = 0;
		int m_numElements = 0;
	};

}

//namespace GameEngine
//{
//	class Debug_renderer
//	{
//	public:
//		Debug_renderer();
//		~Debug_renderer();
//
//		void init();
//		void end();
//		void render(const glm::mat4& matrix_position, float line_width);
//		void dispose();
//
//		void draw_box(const glm::vec4& dest_rect, const GameEngine::ColorRGBA8& color, float angle);
//		void draw_circle(const glm::vec2& center, const GameEngine::ColorRGBA8& color, float radius);
//
//		struct Debug_vertex {
//			glm::vec2 position;
//			GameEngine::ColorRGBA8 color;
//		};
//
//	private:
//		void init_box_debug_vertices(int vsize,const glm::vec4 & dest_rect, const GameEngine::ColorRGBA8 & color, float angle);
//		void init_box_indices(int vsize);
//		void add_same_indices(int index, short number_of_indices);
//
//		void init_circle_vertices(const glm::vec2& center, const GameEngine::ColorRGBA8& color, float radius);
//		void init_circle_indices();
//
//		GameEngine::GLSLProgram m_program;
//		std::vector<Debug_vertex> m_debug_vertices;
//		std::vector<GLubyte> m_indices;
//		GLuint m_vbo=0, m_vao=0, m_ibo=0;
//		int m_num_elements = 0;
//	};
//}
#include"Errors.h"
#include<iostream>

namespace GameEngine {
	void fatal_error(std::string error_message) {
		std::cout << error_message << std::endl;
		std::cout << "Enter quit" << std::endl;
		int _tmp;
		std::cin >> _tmp;
		exit(1);
	}
}
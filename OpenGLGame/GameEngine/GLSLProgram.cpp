#include "GLSLProgram.h"
#include "Errors.h"
#include "IOManager.h"
#include<fstream>
#include<vector>

namespace GameEngine {

	GLSLProgram::GLSLProgram() :m_number_of_attributes(0), m_program_id(0), m_vertex_shader_id(0), m_fragment_shader_id(0)
	{

	}


	GLSLProgram::~GLSLProgram()
	{
	}

	void GLSLProgram::compile_shaders(const std::string & vertex_shader_file_path, const std::string & fragment_shader_file_path)
	{
		std::string vert_source;
		std::string frag_source;

		IOManager::read_file_to_buffer(vertex_shader_file_path, vert_source);
		IOManager::read_file_to_buffer(fragment_shader_file_path, frag_source);

		compile_shaders_from_source(vert_source.c_str(), frag_source.c_str());
	}

	void GLSLProgram::compile_shaders_from_source(const char * vertex_source, const char * fragment_source)
	{
		m_program_id = glCreateProgram();

		m_vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
		if (m_vertex_shader_id == 0) {
			fatal_error("Vertex shader failed to be created!");
		}

		m_fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
		if (m_fragment_shader_id == 0) {
			fatal_error("Fragment shader failed to be created!");
		}

		compile_shader(vertex_source, "Vertex Shader", m_vertex_shader_id);
		compile_shader(fragment_source, "Fragment Shader", m_fragment_shader_id);
	}

	void GLSLProgram::compile_shader(const char* source, const std::string& file_name, GLuint& shader_id)
	{
		glShaderSource(shader_id, 1, &source, nullptr);

		glCompileShader(shader_id);

		GLint is_compiled = 0;
		glGetShaderiv(shader_id, GL_COMPILE_STATUS, &is_compiled);

		if (is_compiled == GL_FALSE)
		{
			GLint max_length = 0;
			glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &max_length);

			// The maxLength includes the NULL character
			std::vector<GLchar> error_log(max_length);
			glGetShaderInfoLog(shader_id, max_length, &max_length, &error_log[0]);

			// Provide the infolog in whatever manor you deem best.
			// Exit with failure.
			glDeleteShader(shader_id); // Don't leak the shader.

			std::printf("%s\n", &error_log[0]);
			fatal_error("Shader " + file_name + " filed to compile");
			return;
		}
	}

	void GLSLProgram::link_shaders()
	{
		//Attach our shaders to our program
		glAttachShader(m_program_id, m_vertex_shader_id);
		glAttachShader(m_program_id, m_fragment_shader_id);

		//Link our program
		glLinkProgram(m_program_id);

		//Note the different functions here: glGetProgram* instead of glGetShader*.
		GLint is_linked = 0;
		glGetProgramiv(m_program_id, GL_LINK_STATUS, (int *)&is_linked);
		if (is_linked == GL_FALSE)
		{
			GLint max_length = 0;
			glGetProgramiv(m_program_id, GL_INFO_LOG_LENGTH, &max_length);

			//The maxLength includes the NULL character
			std::vector<GLchar> error_log(max_length);
			glGetProgramInfoLog(m_program_id, max_length, &max_length, &error_log[0]);

			//We don't need the program anymore.
			glDeleteProgram(m_program_id);
			//Don't leak shaders either.
			glDeleteShader(m_vertex_shader_id);
			glDeleteShader(m_fragment_shader_id);

			std::printf("%s\n", &error_log[0]);
			fatal_error("Shader filed to linked!");
		}

		//Always detach shaders after a successful link.
		glDetachShader(m_program_id, m_vertex_shader_id);
		glDetachShader(m_program_id, m_fragment_shader_id);

		glDeleteShader(m_vertex_shader_id);
		glDeleteShader(m_fragment_shader_id);
	}

	void GLSLProgram::add_attribute(const std::string & attribute_name)
	{
		glBindAttribLocation(m_program_id, m_number_of_attributes++, attribute_name.c_str());
	}

	GLint GLSLProgram::get_uniform_location(const std::string &uniform_name)
	{
		GLint _location = glGetUniformLocation(m_program_id, uniform_name.c_str());

		if (_location == GL_INVALID_INDEX)
		{
			fatal_error("Uniform" + uniform_name + "not found in shader");
		}
		return _location;
	}

	void GLSLProgram::use()
	{
		glUseProgram(m_program_id);
		for (int i = 0; i < m_number_of_attributes; i++) {
			glEnableVertexAttribArray(i);
		}
	}

	void GLSLProgram::unuse()
	{
		glUseProgram(0);
		for (int i = 0; i < m_number_of_attributes; i++) {
			glDisableVertexAttribArray(i);
		}
	}
	void GLSLProgram::dispose()
	{
		if(m_program_id)
			glDeleteProgram(m_program_id);
	}
}
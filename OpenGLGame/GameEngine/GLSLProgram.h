#pragma once

#include<string>
#include<GL/glew.h>

namespace GameEngine {
	class GLSLProgram
	{
	public:
		GLSLProgram();
		~GLSLProgram();

		void compile_shaders(const std::string& vertex_shader_file_path, const std::string& fragment_shader_file_path);

		void compile_shaders_from_source(const char* vertex_source, const char* fragment_source);

		void link_shaders();

		void add_attribute(const std::string& attribute_name);

		GLint get_uniform_location(const std::string &uniform_name);

		void use();

		void unuse();

		void dispose();
	private:

		void compile_shader(const char* source,const std::string& file_name, GLuint& shader_id);

		int m_number_of_attributes;

		GLuint m_program_id;

		GLuint m_vertex_shader_id;
		GLuint m_fragment_shader_id;
	};
}
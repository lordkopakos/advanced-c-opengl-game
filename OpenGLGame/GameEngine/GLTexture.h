#pragma once

#include<gl/glew.h>

namespace GameEngine {
	struct GLTexture {
		GLuint id;
		int width;
		int height;
	};
}
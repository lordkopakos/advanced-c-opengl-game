#include "GUI.h"
#include <SDL\SDL_timer.h>
namespace GameEngine {

	/************************************************************************
	Translate a SDLKey to the proper CEGUI::Key
	*************************************************************************/
	CEGUI::Key::Scan SDLKeyToCEGUIKey(SDL_Keycode key)
	{
		using namespace CEGUI;
		switch (key)
		{
		case SDLK_BACKSPACE:    return Key::Backspace;
		case SDLK_TAB:          return Key::Tab;
		case SDLK_RETURN:       return Key::Return;
		case SDLK_PAUSE:        return Key::Pause;
		case SDLK_ESCAPE:       return Key::Escape;
		case SDLK_SPACE:        return Key::Space;
		case SDLK_COMMA:        return Key::Comma;
		case SDLK_MINUS:        return Key::Minus;
		case SDLK_PERIOD:       return Key::Period;
		case SDLK_SLASH:        return Key::Slash;
		case SDLK_0:            return Key::Zero;
		case SDLK_1:            return Key::One;
		case SDLK_2:            return Key::Two;
		case SDLK_3:            return Key::Three;
		case SDLK_4:            return Key::Four;
		case SDLK_5:            return Key::Five;
		case SDLK_6:            return Key::Six;
		case SDLK_7:            return Key::Seven;
		case SDLK_8:            return Key::Eight;
		case SDLK_9:            return Key::Nine;
		case SDLK_COLON:        return Key::Colon;
		case SDLK_SEMICOLON:    return Key::Semicolon;
		case SDLK_EQUALS:       return Key::Equals;
		case SDLK_LEFTBRACKET:  return Key::LeftBracket;
		case SDLK_BACKSLASH:    return Key::Backslash;
		case SDLK_RIGHTBRACKET: return Key::RightBracket;
		case SDLK_a:            return Key::A;
		case SDLK_b:            return Key::B;
		case SDLK_c:            return Key::C;
		case SDLK_d:            return Key::D;
		case SDLK_e:            return Key::E;
		case SDLK_f:            return Key::F;
		case SDLK_g:            return Key::G;
		case SDLK_h:            return Key::H;
		case SDLK_i:            return Key::I;
		case SDLK_j:            return Key::J;
		case SDLK_k:            return Key::K;
		case SDLK_l:            return Key::L;
		case SDLK_m:            return Key::M;
		case SDLK_n:            return Key::N;
		case SDLK_o:            return Key::O;
		case SDLK_p:            return Key::P;
		case SDLK_q:            return Key::Q;
		case SDLK_r:            return Key::R;
		case SDLK_s:            return Key::S;
		case SDLK_t:            return Key::T;
		case SDLK_u:            return Key::U;
		case SDLK_v:            return Key::V;
		case SDLK_w:            return Key::W;
		case SDLK_x:            return Key::X;
		case SDLK_y:            return Key::Y;
		case SDLK_z:            return Key::Z;
		case SDLK_DELETE:       return Key::Delete;
		case SDLK_KP_0:         return Key::Numpad0;
		case SDLK_KP_1:         return Key::Numpad1;
		case SDLK_KP_2:         return Key::Numpad2;
		case SDLK_KP_3:         return Key::Numpad3;
		case SDLK_KP_4:         return Key::Numpad4;
		case SDLK_KP_5:         return Key::Numpad5;
		case SDLK_KP_6:         return Key::Numpad6;
		case SDLK_KP_7:         return Key::Numpad7;
		case SDLK_KP_8:         return Key::Numpad8;
		case SDLK_KP_9:         return Key::Numpad9;
		case SDLK_KP_PERIOD:    return Key::Decimal;
		case SDLK_KP_DIVIDE:    return Key::Divide;
		case SDLK_KP_MULTIPLY:  return Key::Multiply;
		case SDLK_KP_MINUS:     return Key::Subtract;
		case SDLK_KP_PLUS:      return Key::Add;
		case SDLK_KP_ENTER:     return Key::NumpadEnter;
		case SDLK_KP_EQUALS:    return Key::NumpadEquals;
		case SDLK_UP:           return Key::ArrowUp;
		case SDLK_DOWN:         return Key::ArrowDown;
		case SDLK_RIGHT:        return Key::ArrowRight;
		case SDLK_LEFT:         return Key::ArrowLeft;
		case SDLK_INSERT:       return Key::Insert;
		case SDLK_HOME:         return Key::Home;
		case SDLK_END:          return Key::End;
		case SDLK_PAGEUP:       return Key::PageUp;
		case SDLK_PAGEDOWN:     return Key::PageDown;
		case SDLK_F1:           return Key::F1;
		case SDLK_F2:           return Key::F2;
		case SDLK_F3:           return Key::F3;
		case SDLK_F4:           return Key::F4;
		case SDLK_F5:           return Key::F5;
		case SDLK_F6:           return Key::F6;
		case SDLK_F7:           return Key::F7;
		case SDLK_F8:           return Key::F8;
		case SDLK_F9:           return Key::F9;
		case SDLK_F10:          return Key::F10;
		case SDLK_F11:          return Key::F11;
		case SDLK_F12:          return Key::F12;
		case SDLK_F13:          return Key::F13;
		case SDLK_F14:          return Key::F14;
		case SDLK_F15:          return Key::F15;
		case SDLK_NUMLOCKCLEAR:	return Key::NumLock;
		case SDLK_SCROLLLOCK:	return Key::ScrollLock;
		case SDLK_RSHIFT:       return Key::RightShift;
		case SDLK_LSHIFT:       return Key::LeftShift;
		case SDLK_RCTRL:        return Key::RightControl;
		case SDLK_LCTRL:        return Key::LeftControl;
		case SDLK_RALT:         return Key::RightAlt;
		case SDLK_LALT:         return Key::LeftAlt;
		case SDLK_LGUI:			return Key::LeftWindows;
		case SDLK_RGUI:			return Key::RightWindows;
		case SDLK_SYSREQ:       return Key::SysRq;
		case SDLK_MENU:         return Key::AppMenu;
		case SDLK_POWER:        return Key::Power;
		}
	}

	CEGUI::MouseButton SDL_to_CEGUI_mouse_btn(Uint8 sdl_btn)
	{
		switch (sdl_btn)
		{
		case SDL_BUTTON_LEFT: return CEGUI::LeftButton;
		case SDL_BUTTON_MIDDLE: return CEGUI::MiddleButton;
		case SDL_BUTTON_RIGHT: return CEGUI::RightButton;
		case SDL_BUTTON_X1: return CEGUI::X1Button;
		case SDL_BUTTON_X2: return CEGUI::X2Button;
		default: return CEGUI::NoButton;
		}
	}

	/************************************************************************
	End of translate a SDLKey to the proper CEGUI::Key
	*************************************************************************/

	CEGUI::OpenGL3Renderer* GUI::m_renderer = nullptr;

	GUI::GUI(std::string_view resource_path)
	{
		if (m_renderer == nullptr)
		{
			m_renderer = &CEGUI::OpenGL3Renderer::bootstrapSystem();
			m_renderer->setupRenderingBlendMode(CEGUI::BM_NORMAL, true);
		}

		set_resource_group_directories(resource_path, { "imagesets" , "schemes", "fonts", "layouts", "lua_scripts" });

		set_default_resource_group();

		set_context();
		
	}

	void GUI::set_resource_group_directories(std::string_view resource_path, Resource_directories&& resource_directories)
	{
		CEGUI::DefaultResourceProvider* _resource_provider = static_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());

		for (const auto& resource_directory : resource_directories)
			set_resource_group_directory(_resource_provider, resource_path, resource_directory);

		set_looknfeels_group_directory(resource_path, _resource_provider);
	}

	void GUI::set_resource_group_directory(CEGUI::DefaultResourceProvider* resource_provider, std::string_view resource_path, std::string_view resource_directory)
	{
		std::string _full_path = resource_path.data();
		_full_path += "/" + std::string(resource_directory.data()) + "/";

		resource_provider->setResourceGroupDirectory(resource_directory.data(), _full_path);
	}

	void GUI::set_looknfeels_group_directory(std::string_view resource_path, CEGUI::DefaultResourceProvider * resource_provider)
	{
		std::string _full_path = resource_path.data();
		_full_path += "/looknfeel/";
		resource_provider->setResourceGroupDirectory("looknfeels", _full_path);
	}

	void GUI::set_default_resource_group()
	{
		CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
		CEGUI::Scheme::setDefaultResourceGroup("schemes");
		CEGUI::Font::setDefaultResourceGroup("fonts");
		CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
		CEGUI::WindowManager::setDefaultResourceGroup("layouts");
		CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");
	}

	void GUI::set_context()
	{
		m_context = &CEGUI::System::getSingleton().createGUIContext(m_renderer->getDefaultRenderTarget());
		m_root = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "root");
		m_context->setRootWindow(m_root);
	}

	GUI::~GUI()
	{
		CEGUI::System::getSingleton().destroyGUIContext(*m_context);
	}

	void GUI::draw()
	{
		m_renderer->beginRendering();
		m_context->draw();
		m_renderer->endRendering();
		glDisable(GL_SCISSOR_TEST);
	}

	void GUI::update()
	{
		unsigned int _elapsed;
		if (m_last_time == 0)
		{
			_elapsed = 0;
			m_last_time = SDL_GetTicks();
		}
		else {
			unsigned int _next_time = SDL_GetTicks();
			_elapsed = _next_time - m_last_time;
			m_last_time = _next_time;
		}
		m_context->injectTimePulse(static_cast<float>(_elapsed)/1000.0f);
	}


	//TODO:MouseCursorClass
	void GUI::set_mouse_cursor(std::string_view image_file)
	{
		//TODO:try/catch
		m_context->getMouseCursor().setDefaultImage(image_file.data());
	}

	void GUI::show_mouse_cursor()
	{
		m_context->getMouseCursor().show();
	}

	void GUI::hide_mose_curosr()
	{
		m_context->getMouseCursor().hide();
	}

	void GUI::on_STD_event(SDL_Event & evnt)
	{
		CEGUI::utf32 _code_point;

		switch (evnt.type) 
		{
		case SDL_MOUSEMOTION:
			m_context->injectMousePosition(evnt.motion.x, evnt.motion.y);
			break;
		case SDL_KEYDOWN:
			m_context->injectKeyDown(SDLKeyToCEGUIKey(evnt.key.keysym.sym));
			break;
		case SDL_KEYUP:
			m_context->injectKeyUp(SDLKeyToCEGUIKey(evnt.key.keysym.sym));
			break;
		case SDL_TEXTINPUT:
			_code_point = 0;

			//TODO:
			//std::string _evnt_text = std::string(evnt.text.text);
			//std::vector<int> _utf32_result;
			//utf8::utf8to32(evnt.text.text, evnt.text.text + _evnt_text.size(), std::back_inserter(_utf_32_result));
			//_code_point = static_cast<CEGUI::utf32>(_utf32_result[0]);
			//m_context->injectChar(_code_point);

			for (int i = 0; evnt.text.text[i] != '\0'; i++)
				_code_point |= (((CEGUI::utf32)*(unsigned char*)&evnt.text.text[i]) << (i * 8)); //Why its working!?
				//_code_point |= (static_cast<CEGUI::utf32>(evnt.text.text[i]) << (i * 8));

			m_context->injectChar(_code_point);
			break;
		case SDL_MOUSEBUTTONDOWN:
			m_context->injectMouseButtonDown(SDL_to_CEGUI_mouse_btn(evnt.button.button));
			break;
		case SDL_MOUSEBUTTONUP:
			m_context->injectMouseButtonUp(SDL_to_CEGUI_mouse_btn(evnt.button.button));
			break;
		}
	}

	void GUI::load_scheme(std::string_view scheme_file)
	{
		CEGUI::SchemeManager::getSingleton().createFromFile(scheme_file.data());
	}
	void GUI::load_font(std::string_view font_file)
	{
		std::string full_font_file = font_file.data();
		full_font_file+=".font";

		CEGUI::FontManager::getSingleton().createFromFile(std::move(full_font_file));
		m_context->setDefaultFont(font_file.data());
	}
	CEGUI::Window* GUI::create_widget(std::string_view type, const Widget_property& widget_property, std::string_view name)
	{
		auto new_window = CEGUI::WindowManager::getSingleton().createWindow(type.data(), name.data());
		m_root->addChild(new_window);

		set_widget_dest_rect(new_window, widget_property);

		return new_window;
	}
	void GUI::set_widget_dest_rect(CEGUI::Window* widget, const Widget_property & widget_property)
	{
		const glm::vec4& dest_rect_perc = widget_property.get_dest_rect_perc();
		const glm::vec4& dest_rect_pix = widget_property.get_dest_rect_pix();

		widget->setPosition(CEGUI::UVector2(CEGUI::UDim(dest_rect_perc.x, dest_rect_pix.x), CEGUI::UDim(dest_rect_perc.y, dest_rect_pix.y)));
		widget->setSize(CEGUI::USize(CEGUI::UDim(dest_rect_perc.z, dest_rect_pix.z), CEGUI::UDim(dest_rect_perc.w, dest_rect_pix.w)));
	}
}
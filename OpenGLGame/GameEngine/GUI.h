#pragma once

#include <CEGUI\CEGUI.h>
#include <CEGUI\RendererModules\OpenGL\GL3Renderer.h>

#include<glm\glm.hpp>

#include<SDL\SDL_events.h>

#include <string_view>
#include <memory>

namespace GameEngine {

	struct Widget_property {
		Widget_property(const glm::vec4& dest_rect_perc, const glm::vec4& dest_rect_pix)
			: m_dest_rect_perc(dest_rect_perc), m_dest_rect_pix(dest_rect_pix) {}
		
		const glm::vec4& get_dest_rect_perc() const { return m_dest_rect_perc; }
		const glm::vec4& get_dest_rect_pix() const { return m_dest_rect_pix; }
	private:
		glm::vec4 m_dest_rect_perc; 
		glm::vec4 m_dest_rect_pix;
	};

	using Resource_directories = std::initializer_list<std::string_view>;
	class GUI
	{
	public:
		GUI(std::string_view resource_path);
		~GUI();

		void draw();
		void update();

		void set_mouse_cursor(std::string_view image_file);
		void show_mouse_cursor();
		void hide_mose_curosr();

		void on_STD_event(SDL_Event& evnt);

		void load_scheme(std::string_view scheme_file);
		void load_font(std::string_view font_file);

		CEGUI::Window* create_widget(std::string_view type, const Widget_property& widget_property, std::string_view name = "");
		void set_widget_dest_rect(CEGUI::Window* widget, const Widget_property& widget_property);

		const CEGUI::OpenGL3Renderer* get_renderer() {return m_renderer; }
		CEGUI::GUIContext* get_context() { return m_context; }

	private:

		void set_resource_group_directories(std::string_view resource_path, Resource_directories&& resource_directories);
		void set_resource_group_directory(CEGUI::DefaultResourceProvider* resource_provider, std::string_view resource_path, std::string_view resource_directory);
		void set_looknfeels_group_directory(std::string_view resource_path, CEGUI::DefaultResourceProvider* resource_provider);

		void set_default_resource_group();
		void set_context();
		static CEGUI::OpenGL3Renderer* m_renderer;
		CEGUI::GUIContext* m_context = nullptr;
		CEGUI::Window* m_root = nullptr;
		unsigned int m_last_time = 0;
	};

}

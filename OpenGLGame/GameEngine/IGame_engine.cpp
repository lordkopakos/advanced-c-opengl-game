#include "IGame_engine.h"
#include "Timinig.h"
#include "IScreen.h"
namespace GameEngine {

	IGame_engine::IGame_engine() : m_screen_list(std::make_unique<Screen_list>(this))
	{
		//Empty
	}

	IGame_engine::~IGame_engine()
	{
		//Empty
	}

	void IGame_engine::run()
	{
		if (!init()) return;

		FPS_Limiter fps_limiter;
		fps_limiter.set_max_FPS(120.0f);

		m_runnable = true;
		while (is_running())
		{
			fps_limiter.begin();

			m_input_manager.update();
			update();
			if (m_runnable)
			{
				draw();

				m_fps = fps_limiter.end();
				m_window.swap_buffer();
			}
		}
	}

	void IGame_engine::exit()
	{
		m_current_screen->on_exit();
		if (m_screen_list)
		{
			m_screen_list->destroy();
			m_screen_list.reset();
		}
		m_runnable = false;
	}

	void IGame_engine::update()
	{
		if (m_current_screen)
		{
			switch (m_current_screen->get_state())
			{
			case Screen_state::RUNNING:
				m_current_screen->update();
				break;
			case Screen_state::CHANGE_NEXT:
				change_screen(&Screen_list::move_next);
				break;
			case Screen_state::CHANGE_PREVIOUS:
				change_screen(&Screen_list::move_previous);
				break;
			case Screen_state::EXIT_APPLICATION:
				exit();
			default:
				break;
			}
		}
		else
			exit();
	}

	void IGame_engine::change_screen(Change_screen_func_ptr change_screen_func_ptr)
	{
		m_current_screen->on_exit();
		m_current_screen = (m_screen_list.get()->*change_screen_func_ptr)();
		if (m_current_screen)
		{
			m_current_screen->set_state(Screen_state::RUNNING);
			m_current_screen->on_entry();
		}
	}

	void IGame_engine::draw()
	{
		glViewport(0, 0, m_window.get_screen_width(), m_window.get_screen_height());

		if (m_current_screen && m_current_screen->get_state() == Screen_state::RUNNING)
			m_current_screen->draw();
	}

	bool IGame_engine::init()
	{
		GameEngine::init();

		SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

		if (!init_system()) return false;

		on_init();
		add_screen();

		m_current_screen = m_screen_list->get_current_screen();
		m_current_screen->on_entry();
		m_current_screen->set_state(Screen_state::RUNNING);

		return true;
	}

	bool IGame_engine::init_system()
	{
		m_window.create("Abra-vodkabra", 1600, 900, 0);
		return true;
	}

	void IGame_engine::on_SDL_event(SDL_Event & evnt)
	{
		switch (evnt.type)
		{
		case SDL_QUIT:
			exit();
			break;
		case SDL_KEYDOWN:
			m_input_manager.press_key(evnt.key.keysym.sym);
			break;
		case SDL_KEYUP:
			m_input_manager.release_key(evnt.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			m_input_manager.press_key(evnt.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			m_input_manager.release_key(evnt.button.button);
			break;
		case SDL_MOUSEMOTION:
			m_input_manager.set_mouse_coordinates(static_cast<float>(evnt.motion.x), static_cast<float>(evnt.motion.y));
			break;

		default:
			break;
		}
	}

}
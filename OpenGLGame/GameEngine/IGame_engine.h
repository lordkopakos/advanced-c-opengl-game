#pragma once

#include"SDL_init.h"
#include"Window.h"
#include"Input_Manager.h"
#include "Screen_list.h"

#include<memory>

namespace GameEngine {

	class IScreen;

	using Screen_list_ptr = std::unique_ptr<Screen_list>;
	using IGame_screen_ptr = std::shared_ptr<IScreen>;

	using Change_screen_func_ptr = IGame_screen_ptr (Screen_list::*)();

	class IGame_engine
	{
	public:
		IGame_engine();
		virtual ~IGame_engine();

		//Run and initioalizes the game
		void run();
		//Exits the game
		void exit();

		//Called on initialization
		virtual void on_init() = 0;
		//For adding all screens
		virtual void add_screen() = 0;
		//Called when exiting
		virtual void on_exit() = 0;

		Input_Manager& get_input_manager() { return m_input_manager; }
		constexpr float get_fps() const { return m_fps; }

		void on_SDL_event(SDL_Event& evnt);

	protected:
		//Custom update funcion
		virtual void update();
		//Custom render function
		virtual void draw();

		bool init();
		bool init_system();

		bool is_running() { return m_runnable; }

		void change_screen(Change_screen_func_ptr change_screen_func_ptr);

		Screen_list_ptr m_screen_list =nullptr;
		IGame_screen_ptr m_current_screen=nullptr;
		bool m_runnable=false;
		float m_fps = 0.0f;
		Window m_window;
		Input_Manager m_input_manager;

	};

}

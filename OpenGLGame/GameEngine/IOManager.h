#pragma once
#include<vector>
#include<fstream>

namespace GameEngine {
	class IOManager
	{
	public:
		template<typename String>
		static bool read_file_to_buffer(std::string file_path, String& buffer);
	};

	template<typename String>
	inline bool IOManager::read_file_to_buffer(std::string file_path, String & buffer)
	{
		std::ifstream _file{ file_path, std::ios::binary };
		if (_file.fail()) {
			perror(file_path.c_str());
			return false;
		}

		//seek to the end
		_file.seekg(0, std::ios::end);

		//Get the file size 
		auto _file_size = _file.tellg();
		_file.seekg(0, std::ios::beg);

		//Reduce the file sieze by any header bytes that might be present
		_file_size -= _file.tellg();

		buffer.resize(_file_size);
		_file.read((char *)&(buffer[0]), _file_size);
		_file.close();

		return true;
	}
}

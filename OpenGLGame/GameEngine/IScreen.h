#pragma once

#include<memory>

namespace GameEngine {

	constexpr short NO_SCREEN_INDEX = -1;
	
	class IGame_engine;
	using Main_game_ptr = std::shared_ptr<IGame_engine>;
	
	enum class Screen_state {
		NONE,
		RUNNING,
		EXIT_APPLICATION,
		CHANGE_NEXT,
		CHANGE_PREVIOUS
	};

	class IScreen
	{
	public:
		IScreen()=default;
		virtual ~IScreen()=default;
		//Return the index of the next or previous screen when changing screens
		virtual int get_next_screen_index() const = 0;
		virtual int get_previous_screen_index() const = 0;

		//Called at beginning and end of application
		virtual void build()=0;
		virtual void destroy()=0;

		//Called when a screen enters and exits focus
		virtual void on_entry() = 0;
		virtual void on_exit() = 0;

		//Called in main game loop
		virtual void update() = 0;
		virtual void draw() = 0;

		//Getters
		short get_index() const { return m_index; }
		Screen_state get_state() const { return m_current_state; }

		//Setters
		void set_index(short index) { m_index = index; }
		void set_main_game( IGame_engine* game) { m_game = game; }
		void set_state(Screen_state screen_state) { m_current_state = screen_state; }

	protected:
		Screen_state m_current_state = Screen_state::NONE;
		IGame_engine* m_game = nullptr;
		short m_index = -1;
	};
}

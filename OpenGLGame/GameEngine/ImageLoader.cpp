#include "ImageLoader.h"
#include"picoPNG.h"
#include"IOManager.h"
#include"Errors.h"

namespace GameEngine {

	GLTexture ImageLoader::load_PNG(std::string file_path)
	{

		GLTexture _texture = {};

		std::vector<unsigned char> _in;
		std::vector<unsigned char> _out;

		unsigned long _width, _height;

		if (IOManager::read_file_to_buffer(file_path, _in) == false) {
			fatal_error("Failed to load PNG file to buffer");
		}

		int _error_code = decode_PNG(_out, _width, _height, &(_in[0]), _in.size());

		if (_error_code != 0) {
			fatal_error("decode_PNG failed with error: " + std::to_string(_error_code));
		}

		glGenTextures(1, &(_texture.id));

		glBindTexture(GL_TEXTURE_2D, _texture.id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &(_out[0]));

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);

		glBindTexture(GL_TEXTURE_2D, 0);

		_texture.width = _width;
		_texture.height = _height;

		return _texture;
	}
}
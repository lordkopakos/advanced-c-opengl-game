#pragma once

#include"GLTexture.h"
#include<string>

namespace GameEngine {
	class ImageLoader
	{
	public:
		static GLTexture load_PNG(std::string file_path);
	};
}


#include "Input_Manager.h"


namespace GameEngine {
	Input_Manager::Input_Manager() : m_mouse_coordinates(0.0f)
	{
	}


	Input_Manager::~Input_Manager()
	{
	}

	void Input_Manager::update()
	{
		for (auto& map_itr : m_key_map)
			m_previous_key_map[map_itr.first] = map_itr.second;
	}

	void GameEngine::Input_Manager::press_key(unsigned int key_id)
	{
		m_key_map[key_id] = true;
	}
	void Input_Manager::release_key(unsigned int key_id)
	{
		m_key_map[key_id] = false;
	}
	bool Input_Manager::is_key_down(unsigned int key_id)
	{
		auto map_iterator = m_key_map.find(key_id);
		
		if (map_iterator != m_key_map.end())
			return map_iterator->second;
		else
			return false;
	}
	bool Input_Manager::is_key_pressed(unsigned int key_id)
	{
		if(is_key_down(key_id) == true && was_key_down(key_id)==false)
			return true;

		return false;
	}
	void Input_Manager::set_mouse_coordinates(float x, float y)
	{
		m_mouse_coordinates.x = x;
		m_mouse_coordinates.y = y;
	}
	bool Input_Manager::was_key_down(unsigned int key_id)
	{
		auto map_iterator = m_previous_key_map.find(key_id);

		if (map_iterator != m_previous_key_map.end())
			return map_iterator->second;
		else
			return false;
	}
}
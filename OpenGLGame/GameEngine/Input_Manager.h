#pragma once

#include<unordered_map>
#include<glm\glm.hpp>

namespace GameEngine {
	class Input_Manager
	{
	public:
		Input_Manager();
		~Input_Manager();

		void update();

		void press_key(unsigned int key_id);
		void release_key(unsigned int key_id);

		bool is_key_down(unsigned int key_id);
		bool is_key_pressed(unsigned int key_id);

		//setters
		void set_mouse_coordinates(float x, float y);

		//getters
		glm::vec2 get_mouse_coordinates() const { return m_mouse_coordinates; }
	private:
		bool was_key_down(unsigned int key_id);

		std::unordered_map<unsigned int, bool> m_key_map;
		std::unordered_map<unsigned int, bool> m_previous_key_map;
		glm::vec2 m_mouse_coordinates;
	};
}


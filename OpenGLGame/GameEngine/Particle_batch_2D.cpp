#include "Particle_batch_2D.h"

namespace GameEngine {

	Particle_batch_2D::Particle_batch_2D() :m_decay_rate(0.1f), m_particles(nullptr), m_max_particles(0), m_last_free_particle(0)
	{
		//Empty
	}


	Particle_batch_2D::~Particle_batch_2D()
	{
	}

	void Particle_batch_2D::init(int max_particles, float decay_rate, GLTexture texture, Update_function update_function/* = default_particle_update*/)
	{
		m_max_particles = max_particles;
		m_particles=std::unique_ptr<Particle2D[]>(new Particle2D[max_particles]());
		m_decay_rate = decay_rate;
		m_texture = texture;
		m_update_function = update_function;
	}

	void Particle_batch_2D::update(float delta_time)
	{
		for (int particle_index = 0; particle_index < m_max_particles; particle_index++)
		{
			auto& _current_particle = m_particles[particle_index];
			if (_current_particle.life > 0.0f)
			{
				m_update_function(_current_particle, delta_time);
				_current_particle.life -= m_decay_rate * delta_time;
			}
		}
	}

	void Particle_batch_2D::draw(SpriteBatch& sprite_batch)
	{
		glm::vec4 _UV_rect(0.0f, 0.0f, 1.0f, 1.0f);
		for (int particle_index = 0; particle_index < m_max_particles; particle_index++)
		{
			auto& current_particle = m_particles[particle_index];
			if (current_particle.life > 0.0f)
			{
				glm::vec4 _dest_rect(current_particle.position.x, current_particle.position.y, current_particle.width, current_particle.width);
				sprite_batch.draw(_dest_rect, _UV_rect, m_texture.id, 0.0f, current_particle.color);
			}
		}
	}

	void Particle_batch_2D::add_particle(const glm::vec2 & position, const glm::vec2 & velocity, const ColorRGBA8 & color, float width)
	{
		int _particle_index = find_free_particle();
		auto& _current_particle = m_particles[_particle_index];

		_current_particle.life = 1.0f;
		_current_particle.position = position;
		_current_particle.velocity = velocity;
		_current_particle.color = color;
		_current_particle.width = width;
	}

	int Particle_batch_2D::find_free_particle()
	{
		for (int particle_index = m_last_free_particle; particle_index < m_max_particles; particle_index++)
		{
			auto& current_particle = m_particles[particle_index];
			if (current_particle.life <= 0.0f)
			{
				m_last_free_particle = particle_index;
				return particle_index;
			}
		}
		
		for (int particle_index = 0; particle_index < m_last_free_particle; particle_index++)
		{
			auto& current_particle = m_particles[particle_index];
			if (current_particle.life <= 0.0f)
			{
				m_last_free_particle = particle_index;
				return particle_index;
			}
		}
		//No particles are free, overwrite first particle
		return 0;
	}

}
#pragma once
#include"Vertex.h"
#include"SpriteBatch.h"
#include"GLTexture.h"

#include<glm/glm.hpp>

#include<functional>
#include<memory>
namespace GameEngine {


	struct Particle2D {
		glm::vec2 position = glm::vec2(0.0f);
		glm::vec2 velocity = glm::vec2(0.0f);
		ColorRGBA8 color;
		float life = 0.0f;
		float width = 0.0f;
	};

	inline void default_particle_update(Particle2D& particle_2D, float delta_time)
	{
		particle_2D.position += particle_2D.velocity * delta_time;
	}

	class Particle_batch_2D
	{
		using Update_function = std::function<void(Particle2D&, float)>;

	public:
		Particle_batch_2D();
		~Particle_batch_2D();

		void init(int max_particles, float decay_rate, GLTexture texture, Update_function update_function = default_particle_update);

		void update(float delta_time);

		void draw(SpriteBatch& sprite_batch);

		void add_particle(const glm::vec2& position,
						  const glm::vec2& velocity,
						  const ColorRGBA8& color,
						  float width);

	private:
		int find_free_particle();

		Update_function m_update_function;
		float m_decay_rate;
		std::unique_ptr<Particle2D[]> m_particles;
		int m_max_particles;
		int m_last_free_particle;
		GLTexture m_texture;
	};
}
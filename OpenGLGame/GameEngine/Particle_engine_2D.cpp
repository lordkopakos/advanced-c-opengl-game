#include "Particle_engine_2D.h"
#include"SpriteBatch.h"

namespace GameEngine {

	Particle_engine_2D::Particle_engine_2D()
	{
		//Empty
	}


	Particle_engine_2D::~Particle_engine_2D()
	{
		for (auto& batch : m_batches)
			batch.~shared_ptr();
	}
	void Particle_engine_2D::add_particle_batch(std::shared_ptr<Particle_batch_2D> particle_batch_2D)
	{
		m_batches.push_back(particle_batch_2D);
	}
	void Particle_engine_2D::update(float delta_time)
	{
		for (auto& batch : m_batches) 
			batch->update(delta_time);
	}
	void Particle_engine_2D::draw(SpriteBatch& sprite_batch)
	{
		for (auto& batch : m_batches) {
			sprite_batch.begin();
			batch->draw(sprite_batch);
			sprite_batch.end();
			sprite_batch.render_batch();
		}
	}
}
#pragma once
#include<memory>
#include<vector>
#include"Particle_batch_2D.h"

class SpriteBatch;

namespace GameEngine {

	class Particle_engine_2D
	{
	public:
		Particle_engine_2D();
		~Particle_engine_2D();

		void add_particle_batch(std::shared_ptr<Particle_batch_2D> particle_batch_2D);

		void update(float delta_time);

		void draw(SpriteBatch& sprite_batch);

	private:
		std::vector<std::shared_ptr<Particle_batch_2D>> m_batches;
	};
}

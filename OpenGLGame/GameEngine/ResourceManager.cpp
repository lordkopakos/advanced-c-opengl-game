#include "ResourceManager.h"

namespace GameEngine {

	TextureCache ResourceManager::m_texture_cache;

	GLTexture ResourceManager::get_texture(std::string texture_path)
	{
		return m_texture_cache.get_texture(texture_path);
	}
}
#pragma once
#include"TextureCache.h"
#include"GLTexture.h"
#include<string>

namespace GameEngine {
	class ResourceManager
	{
	public:
		static GLTexture get_texture(std::string texture_path);
	private:
		static TextureCache m_texture_cache;
	};
}
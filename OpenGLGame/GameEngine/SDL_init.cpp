#include<SDL\SDL.h>
#include<GL\glew.h>
#include "SDL_init.h"

namespace GameEngine {
	int init()
	{
		SDL_Init(SDL_INIT_EVERYTHING);

		// Tell SDL that we want a double buffered window so we dont get anny flickering
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		return 0;
	}
}

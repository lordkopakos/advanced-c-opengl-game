#include "Screen_list.h"

namespace GameEngine {

	Screen_list::Screen_list(IGame_engine* game) :m_game(game)
	{
		//Empty
	}

	Screen_list::~Screen_list()
	{
		destroy();
	}

	IGame_screen_ptr Screen_list::move_next()
	{
		
		return move(&IScreen::get_next_screen_index);
	}

	IGame_screen_ptr Screen_list::move_previous()
	{
		return move(&IScreen::get_next_screen_index);
	}

	IGame_screen_ptr Screen_list::move(Screen_index_func_ptr screen_index_function)
	{
		IScreen* _current_screen = get_current_screen().get();
		if ((_current_screen->*screen_index_function)() != NO_SCREEN_INDEX)
			m_current_screen_index = (_current_screen->*screen_index_function)();
		return get_current_screen();
	}

	void Screen_list::set_screen(short next_screen_index)
	{
		m_current_screen_index = next_screen_index;
	}

	void Screen_list::add_screen(IGame_screen_ptr new_screen)
	{
		new_screen->set_index(static_cast<short>(m_screens.size()));
		new_screen->build();
		new_screen->set_main_game(m_game);
		m_screens.emplace_back(new_screen);
	}

	void Screen_list::destroy()
	{
		m_screens.clear();
		m_current_screen_index = NO_SCREEN_INDEX;
	}

	IGame_screen_ptr Screen_list::get_current_screen()
	{
		if (m_current_screen_index == NO_SCREEN_INDEX)
			return nullptr;
		return m_screens[m_current_screen_index];
	}
}
#pragma once

#include "IScreen.h"

#include<vector>
#include<memory>


namespace GameEngine {

	class IGame_engine;

	using IGame_screen_ptr = std::shared_ptr<IScreen>;
	using IMain_game_ptr = std::shared_ptr<IGame_engine>;

	using Screen_index_func_ptr = int(IScreen::*)() const;

	class Screen_list
	{
	public:
		Screen_list(IGame_engine* game);
		~Screen_list();

		IGame_screen_ptr move_next();
		IGame_screen_ptr move_previous();

		void set_screen(short next_screen_index);
		void add_screen(IGame_screen_ptr new_screen);

		void destroy();

		IGame_screen_ptr get_current_screen();

	protected:
		IGame_engine* m_game = nullptr;
		std::vector<IGame_screen_ptr> m_screens;
		int m_current_screen_index = -1;

	private:
		IGame_screen_ptr move(Screen_index_func_ptr screen_index_function);
	};
}

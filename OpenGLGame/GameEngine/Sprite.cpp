#include "Sprite.h"
#include<array>
#include"Vertex.h"
#include<cstddef>

namespace GameEngine {

	Sprite::Sprite()
	{
		m_VBO_id = 0;
	}


	Sprite::~Sprite()
	{
		if (m_VBO_id != 0) {
			glDeleteBuffers(1, &m_VBO_id);
		}
	}

	void Sprite::init(float x, float y, float width, float heigh, std::string texture_path)
	{
		m_x = x;
		m_y = y;
		m_width = width;
		m_height = heigh;

		m_texture = ResourceManager::get_texture(texture_path);

		if (m_VBO_id == 0) {
			glGenBuffers(1, &m_VBO_id);
		}

		std::array<Vertex, 6> _vertex_data;

		//First triangle
		_vertex_data[0].set_position(x + width, y + heigh);
		_vertex_data[0].set_UV_coordinates(1.0f, 1.0f);

		_vertex_data[1].set_position(x, y + heigh);
		_vertex_data[1].set_UV_coordinates(0.0f, 1.0f);

		_vertex_data[2].set_position(x, y);
		_vertex_data[2].set_UV_coordinates(0.0f, 0.0f);

		//Second triangle
		_vertex_data[3].set_position(x, y);
		_vertex_data[3].set_UV_coordinates(0.0f, 0.0f);

		_vertex_data[4].set_position(x + width, y);
		_vertex_data[4].set_UV_coordinates(1.0f, 0.0f);

		_vertex_data[5].set_position(x + width, y + heigh);
		_vertex_data[5].set_UV_coordinates(1.0f, 1.0f);


		for (int i = 0; i < 6; i++) {
			_vertex_data[i].set_color(255, 0, 255, 255);
		}



		_vertex_data[1].set_color(0, 0, 255, 255);

		_vertex_data[4].set_color(0, 255, 0, 255);



		//Tell OpenGL to bind our vertex buffer object
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO_id);
		//Upload the data to the GPU
		glBufferData(GL_ARRAY_BUFFER, sizeof(_vertex_data), &_vertex_data[0], GL_STATIC_DRAW);
		//Unbind the buffer (optional)
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO_id);
	}

	void Sprite::draw()
	{
		glBindTexture(GL_TEXTURE_2D, m_texture.id);

		//bind the buffer object
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO_id);

		//Tell OpenGL that we want to use the first 
		//attribute array. 
		glEnableVertexAttribArray(0);

		glEnableVertexAttribArray(1);

		glEnableVertexAttribArray(2);

		//This is the position attribute pointer
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
		//This is the color attribute pointer
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));
		//This is the UV coordinates attribute pointer
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv_coordinates));

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableVertexAttribArray(0);

		glDisableVertexAttribArray(1);

		glDisableVertexAttribArray(2);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}
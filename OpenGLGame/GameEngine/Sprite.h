#pragma once

#include"GLTexture.h"
#include"ResourceManager.h"

#include<GL/glew.h>
#include<string>

namespace GameEngine {
	class Sprite
	{
	public:
		Sprite();
		~Sprite();

		void init(float x, float y, float width, float heigh, std::string texture_path);
		void draw();

	private:
		int m_x;
		int m_y;
		int m_width;
		int m_height;

		GLuint m_VBO_id;

		GLTexture m_texture;
	};
}

#include "SpriteBatch.h"
#include"Utilities.h"

#include<algorithm>
namespace GameEngine {


	Glyph::Glyph(const glm::vec4 & destination_rectangle, const glm::vec4 & uv_rectangle, GLuint texture, float depth, const ColorRGBA8 & color)
		:texture(texture), depth(depth)
	{
		top_left.set_color(color.r, color.g, color.b, color.a);
		top_left.set_position(destination_rectangle.x, destination_rectangle.y + destination_rectangle.w);
		top_left.set_UV_coordinates(uv_rectangle.x, uv_rectangle.y + uv_rectangle.w);

		bottom_left.set_color(color.r, color.g, color.b, color.a);
		bottom_left.set_position(destination_rectangle.x, destination_rectangle.y);
		bottom_left.set_UV_coordinates(uv_rectangle.x, uv_rectangle.y);

		bottom_right.set_color(color.r, color.g, color.b, color.a);
		bottom_right.set_position(destination_rectangle.x + destination_rectangle.z, destination_rectangle.y);
		bottom_right.set_UV_coordinates(uv_rectangle.x + uv_rectangle.z, uv_rectangle.y);

		top_right.set_color(color.r, color.g, color.b, color.a);
		top_right.set_position(destination_rectangle.x + destination_rectangle.z, destination_rectangle.y + destination_rectangle.w);
		top_right.set_UV_coordinates(uv_rectangle.x + uv_rectangle.z, uv_rectangle.y + uv_rectangle.w);


	};

	Glyph::Glyph(const glm::vec4 & destination_rectangle, const glm::vec4 & uv_rectangle, GLuint texture, float depth, const ColorRGBA8 & color, float angle)
		:texture(texture), depth(depth)
	{
		glm::vec2 half_dims{ destination_rectangle.z / 2.0f, destination_rectangle.w / 2.0f };
		
		//Get points centered at origin
		glm::vec2 tl{ -half_dims.x, half_dims.y };
		glm::vec2 bl{ -half_dims.x, -half_dims.y };
		glm::vec2 br{ half_dims.x, -half_dims.y };
		glm::vec2 tr{ half_dims.x, half_dims.y };

		//Rotate the points minus because is left, not right 
		tl=rotate_point(tl, angle) + half_dims;
		bl=rotate_point(bl, angle) + half_dims;
		br=rotate_point(br, angle) + half_dims;
		tr=rotate_point(tr, angle) + half_dims;

		top_left.set_color(color.r, color.g, color.b, color.a);
		top_left.set_position(destination_rectangle.x + tl.x, destination_rectangle.y + tl.y);
		top_left.set_UV_coordinates(uv_rectangle.x, uv_rectangle.y + uv_rectangle.w);

		bottom_left.set_color(color.r, color.g, color.b, color.a);
		bottom_left.set_position(destination_rectangle.x + bl.x, destination_rectangle.y + bl.y);
		bottom_left.set_UV_coordinates(uv_rectangle.x, uv_rectangle.y);

		top_right.set_color(color.r, color.g, color.b, color.a);
		top_right.set_position(destination_rectangle.x + tr.x, destination_rectangle.y + tr.y);
		top_right.set_UV_coordinates(uv_rectangle.x + uv_rectangle.z, uv_rectangle.y + uv_rectangle.w);

		bottom_right.set_color(color.r, color.g, color.b, color.a);
		bottom_right.set_position(destination_rectangle.x + br.x, destination_rectangle.y + br.y);
		bottom_right.set_UV_coordinates(uv_rectangle.x + uv_rectangle.z, uv_rectangle.y);
	}

	SpriteBatch::SpriteBatch() 
		:m_vbo(0),
		m_vao(0)
	{
	}


	SpriteBatch::~SpriteBatch()
	{

	}
	void SpriteBatch::init()
	{
		create_vertex_array();
	}
	void SpriteBatch::begin(Glyph_Sort_Type glyph_sort_type)
	{
		m_glyph_sort_type = glyph_sort_type;
		m_render_batches.clear();
		m_glyphs.clear();
	}
	void SpriteBatch::end()
	{
		m_glyphs_ptr.resize(m_glyphs.size());
		for (auto i=0; i< m_glyphs.size();i++)
			m_glyphs_ptr[i] = std::make_shared<Glyph>(m_glyphs[i]);

		sort_glyphs();
		create_render_batches();
	}
	void SpriteBatch::draw(const glm::vec4 & destination_rectangle, const glm::vec4 & uv_rectangle, GLuint texture, float depth, const ColorRGBA8 & color)
	{
		m_glyphs.emplace_back(destination_rectangle, uv_rectangle, texture, depth, color);
	}

	void SpriteBatch::draw(const glm::vec4 & destination_rectangle, const glm::vec4 & uv_rectangle, GLuint texture, float depth, const ColorRGBA8 & color, float angle)
	{
		m_glyphs.emplace_back(destination_rectangle, uv_rectangle, texture, depth, color, angle);
	}

	void SpriteBatch::draw(const glm::vec4 & destination_rectangle, const glm::vec4 & uv_rectangle, GLuint texture, float depth, const ColorRGBA8 & color, const glm::vec2& direction)
	{
		const glm::vec2 left(-1.0f, 0.0f);
		float angle = acos(glm::dot(left, direction));

		if (direction.y < 0.0f)
			angle = -angle;

		m_glyphs.emplace_back(destination_rectangle, uv_rectangle, texture, depth, color, angle);
	}

	void SpriteBatch::render_batch()
	{
		glBindVertexArray(m_vao);
		for (auto &render_batch : m_render_batches) {
			glBindTexture(GL_TEXTURE_2D, render_batch.m_texture);
			glDrawArrays(GL_TRIANGLES, render_batch.m_offset, render_batch.m_number_of_verticles);
		}
		glBindVertexArray(0);
	}
	void SpriteBatch::create_render_batches()
	{
		std::vector<Vertex> _vertices;
		_vertices.resize(m_glyphs_ptr.size() * 6);

		if (m_glyphs_ptr.empty())
			return;

		auto _offset = 0;
		auto _current_vertex_index = 0; //current vertex

		//new RenderBatch
		m_render_batches.emplace_back(0, 6,m_glyphs_ptr[0]->texture);

		_vertices[_current_vertex_index++] = m_glyphs_ptr[0]->top_left;
		_vertices[_current_vertex_index++] = m_glyphs_ptr[0]->bottom_left;
		_vertices[_current_vertex_index++] = m_glyphs_ptr[0]->bottom_right;
		_vertices[_current_vertex_index++] = m_glyphs_ptr[0]->bottom_right;
		_vertices[_current_vertex_index++] = m_glyphs_ptr[0]->top_right;
		_vertices[_current_vertex_index++] = m_glyphs_ptr[0]->top_left;
		
		_offset += 6;
		
		/*auto _current_glyps = 1;
		for (auto &current_glyphs : m_glyphs_ptr) //without first
		{
			if (current_glyphs->texture != m_glyphs_ptr[_current_glyps - 1]->texture)
				m_render_batches.emplace_back(0, 6, current_glyphs->texture);
			else 
				m_render_batches.back().m_number_of_verticles += 6;

			_vertices[_current_vertex_index++] = current_glyphs->top_left;
			_vertices[_current_vertex_index++] = current_glyphs->bottom_left;
			_vertices[_current_vertex_index++] = current_glyphs->bottom_right;
			_vertices[_current_vertex_index++] = current_glyphs->bottom_right;
			_vertices[_current_vertex_index++] = current_glyphs->top_right;
			_vertices[_current_vertex_index++] = current_glyphs->top_left;
			
			_current_glyps++;
			_offset += 6;
		}*/

		for (auto current_glyph_index = 1; current_glyph_index<m_glyphs_ptr.size(); current_glyph_index++)
		{
			if (m_glyphs_ptr[current_glyph_index]->texture != m_glyphs_ptr[current_glyph_index - 1]->texture)
				m_render_batches.emplace_back(_offset, 6, m_glyphs_ptr[current_glyph_index]->texture);
			else
				m_render_batches.back().m_number_of_verticles += 6;

			_vertices[_current_vertex_index++] = m_glyphs_ptr[current_glyph_index]->top_left;
			_vertices[_current_vertex_index++] = m_glyphs_ptr[current_glyph_index]->bottom_left;
			_vertices[_current_vertex_index++] = m_glyphs_ptr[current_glyph_index]->bottom_right;
			_vertices[_current_vertex_index++] = m_glyphs_ptr[current_glyph_index]->bottom_right;
			_vertices[_current_vertex_index++] = m_glyphs_ptr[current_glyph_index]->top_right;
			_vertices[_current_vertex_index++] = m_glyphs_ptr[current_glyph_index]->top_left;

			_offset += 6;
		}

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		//orphan the buffer
		glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex), nullptr, GL_STREAM_DRAW);
		//upload the data
		glBufferSubData(GL_ARRAY_BUFFER, 0, _vertices.size() * sizeof(Vertex), _vertices.data());

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void SpriteBatch::create_vertex_array()
	{
		if (m_vao==0)
			glGenVertexArrays(1, &m_vao);

		glBindVertexArray(m_vao);

		if (m_vbo == 0) 
			glGenBuffers(1, &m_vbo);

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

		//Tell OpenGL that we want to use the first 
		//attribute array. 
		glEnableVertexAttribArray(0);

		glEnableVertexAttribArray(1);

		glEnableVertexAttribArray(2);

		//This is the position attribute pointer
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
		//This is the color attribute pointer
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));
		//This is the UV coordinates attribute pointer
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv_coordinates));

		glBindVertexArray(0);
	}
	void SpriteBatch::sort_glyphs()
	{
		switch (m_glyph_sort_type)
		{
		case Glyph_Sort_Type::BACK_TO_FRONT:
			std::stable_sort(m_glyphs_ptr.begin(), m_glyphs_ptr.end(), [](auto a, auto b) {return (a->depth < b->depth);});
			break;
		case Glyph_Sort_Type::FRONT_TO_BACK:
			std::stable_sort(m_glyphs_ptr.begin(), m_glyphs_ptr.end(), [](auto a, auto b) {return (a->depth > b->depth); });
			break;
		case Glyph_Sort_Type::TEXTURE:
			std::stable_sort(m_glyphs_ptr.begin(), m_glyphs_ptr.end(), [](auto a, auto b) {return (a->texture < b->texture); });
			break;
		default:
			break;
		}
		
	}
}
#pragma once

#include"Vertex.h"

#include<GL\glew.h>
#include<glm\glm.hpp>
#include<vector>
#include<memory>

namespace GameEngine {

	enum class Glyph_Sort_Type {
		NONE,
		FRONT_TO_BACK,
		BACK_TO_FRONT,
		TEXTURE
	};

	struct Glyph {
	public:
		Glyph(){};
		Glyph(const glm::vec4 & destination_rectangle, const glm::vec4 & uv_rectangle, GLuint texture, float depth, const ColorRGBA8 & color);
		Glyph(const glm::vec4 & destination_rectangle, const glm::vec4 & uv_rectangle, GLuint texture, float depth, const ColorRGBA8 & color, float angle);

		GLuint texture;

		float depth;

		Vertex top_left;
		Vertex bottom_left;
		Vertex top_right;
		Vertex bottom_right;
	};

	class Render_Batch{
	public:
		Render_Batch(GLuint offset, GLuint number_of_verticles, GLuint texture) :m_offset(offset), m_number_of_verticles(number_of_verticles), m_texture(texture){}
		GLuint m_offset;
		GLuint m_number_of_verticles;
		GLuint m_texture;
	};

	class SpriteBatch
	{
	public:
		SpriteBatch();
		~SpriteBatch();
		
		void init();

		void begin(Glyph_Sort_Type glyph_sort_type = Glyph_Sort_Type::TEXTURE);
		void end();

		void draw(const glm::vec4& destination_rectangle, const glm::vec4& uv_rectangle, GLuint texture, float depth, const ColorRGBA8& color);
		void draw(const glm::vec4& destination_rectangle, const glm::vec4& uv_rectangle, GLuint texture, float depth, const ColorRGBA8& color, float angle);
		void draw(const glm::vec4& destination_rectangle, const glm::vec4& uv_rectangle, GLuint texture, float depth, const ColorRGBA8& color, const glm::vec2& direction);

		void render_batch();

	private:
		void create_render_batches();
		void create_vertex_array();
		void sort_glyphs();

		GLuint m_vbo;
		GLuint m_vao;

		Glyph_Sort_Type m_glyph_sort_type;

		std::vector<std::shared_ptr<Glyph>> m_glyphs_ptr;	//This is for sorting 
		std::vector<Glyph> m_glyphs;						//This is actual Glyphs
		std::vector<Render_Batch> m_render_batches;
	};
}

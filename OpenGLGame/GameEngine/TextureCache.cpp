#include "TextureCache.h"
#include"ImageLoader.h"

#include<iostream>

namespace GameEngine {

	TextureCache::TextureCache()
	{
	}


	TextureCache::~TextureCache()
	{
	}

	GLTexture TextureCache::get_texture(std::string texture_path)
	{
		//lookup the texture and see if its in the map
		auto map_iterator = m_texture_map.find(texture_path);

		//check if its not in the map
		if (map_iterator == m_texture_map.end()) {
			GLTexture _new_texture = ImageLoader::load_PNG(texture_path);
			m_texture_map.insert(std::make_pair(texture_path, _new_texture));

			std::cout << "Loaded Cached Texture!" << std::endl;
			return _new_texture;
		}

		return map_iterator->second;
	}
}
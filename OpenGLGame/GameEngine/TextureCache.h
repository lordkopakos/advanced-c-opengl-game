#pragma once
#include<map>
#include<string>
#include"GLTexture.h"

namespace GameEngine {
	class TextureCache
	{
	public:
		TextureCache();
		~TextureCache();

		GLTexture get_texture(std::string texture_path);
	private:
		std::map<std::string, GLTexture> m_texture_map;
	};
}

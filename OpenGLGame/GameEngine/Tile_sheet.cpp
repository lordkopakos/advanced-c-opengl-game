#include"Tile_sheet.h"
namespace GameEngine {
	void Tile_sheet::init(const GameEngine::GLTexture & texture, const glm::ivec2 & tile_dimensions)
	{
		m_texture = texture;
		m_dimensions = tile_dimensions;
	}

	glm::vec4 Tile_sheet::get_UVs(int index)
	{
		int _x_tile = index % m_dimensions.x;
		int _y_tile = index / m_dimensions.x;

		glm::vec4 _UVs;
		_UVs.x = _x_tile / static_cast<float>(m_dimensions.x);
		_UVs.y = _y_tile / static_cast<float>(m_dimensions.y);
		_UVs.z = 1.0f / m_dimensions.x;
		_UVs.w = 1.0f / m_dimensions.y;

		return _UVs;
	}
}
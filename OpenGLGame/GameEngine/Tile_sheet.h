#pragma once
#include"GLTexture.h"
#include<glm\glm.hpp>
namespace GameEngine {
	class Tile_sheet {
	public:
		void init(const GameEngine::GLTexture& texture, const glm::ivec2& tile_dimensions);
		glm::vec4 get_UVs(int index);

		const GameEngine::GLTexture& get_texture() { return m_texture; }
		const glm::ivec2& get_dimensions() { return m_dimensions; }

	private:
		GameEngine::GLTexture m_texture;
		glm::ivec2 m_dimensions;
	};
}
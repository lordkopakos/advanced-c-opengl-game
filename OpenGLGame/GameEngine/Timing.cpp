#include"Timinig.h"

#include<SDL\SDL.h>
#include<array>

namespace GameEngine {
	FPS_Limiter::FPS_Limiter() 
	{

	}
	void FPS_Limiter::init(float max_FPS)
	{
		set_max_FPS(max_FPS);
	}
	void FPS_Limiter::set_max_FPS(float max_FPS)
	{
		m_max_FPS = max_FPS;
	}
	void FPS_Limiter::begin()
	{
		m_start_ticks = SDL_GetTicks();
	}
	float FPS_Limiter::end()
	{
		calculate_FPS();
		auto _frame_ticks = SDL_GetTicks() - m_start_ticks;
		//Limit the FPS to the max FPS
		if (1000.0f / m_max_FPS > _frame_ticks) {
			SDL_Delay(1000.0f / m_max_FPS - _frame_ticks);
		}
		return m_fps;
	}
	void FPS_Limiter::calculate_FPS()
	{
		static constexpr int _NUM_SAMPLES = 10;
		static std::array<float, _NUM_SAMPLES> _frame_times;
		static int _current_frame = 0;

		static auto _preview_ticks = SDL_GetTicks();

		auto _current_ticks = SDL_GetTicks();

		m_frame_time = _current_ticks - _preview_ticks;
		_frame_times[_current_frame%_NUM_SAMPLES] = m_frame_time;

		_preview_ticks = _current_ticks;

		int _count;

		_current_frame++;
		if (_current_frame < _NUM_SAMPLES) {
			_count = _current_frame;
		}
		else {
			_count = _NUM_SAMPLES;
		}

		float _fram_time_average = 0;
		for (int i = 0; i < _count; i++) {
			_fram_time_average += _frame_times[i];
		}
	
		_fram_time_average /= _count;

		if (_fram_time_average > 0) {
			m_fps = 1000.0f / _fram_time_average;
		}
		else {
			m_fps = 60.0f;
		}
	}
}
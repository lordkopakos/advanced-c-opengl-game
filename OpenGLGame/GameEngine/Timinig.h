#pragma once
namespace GameEngine {
	class FPS_Limiter {
	public:
		FPS_Limiter();
		void init(float max_FPS);

		void set_max_FPS(float max_FPS);

		void begin();
		float end();

	private:
		void calculate_FPS();
		
		float m_fps;
		float m_max_FPS;
		float m_frame_time;
		unsigned int m_start_ticks;
	};
}
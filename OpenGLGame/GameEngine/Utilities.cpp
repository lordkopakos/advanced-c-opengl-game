#include "Utilities.h"

glm::vec2 rotate_point(glm::vec2 position, float angle)
{
	glm::vec2 result_vector;
	result_vector.x = position.x*cos(angle) - position.y*sin(angle);
	result_vector.y = position.x*sin(angle) + position.y*cos(angle);
	return result_vector;
}
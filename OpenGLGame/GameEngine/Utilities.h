#pragma once
#include <glm\glm.hpp>

glm::vec2 rotate_point(glm::vec2 position, float angle);
#pragma once

#include<gl/glew.h>
#include<array>

namespace GameEngine {
	struct Position {
		Position():x(0), y(0) {}
		Position(float x, float y) :x(x), y(y) {}

		float x;
		float y;
	};

	struct ColorRGBA8 {
		ColorRGBA8() : r(0), g(0), b(0), a(0) {}
		ColorRGBA8(GLubyte r, GLubyte g, GLubyte b, GLubyte a) : r(r), g(g), b(b), a(a) {}

		GLubyte r;
		GLubyte g;
		GLubyte b;
		GLubyte a;
	};

	struct UV_Coordinates
	{
		UV_Coordinates() :u(0), v(0) {}
		UV_Coordinates(float u, float v) :u(u), v(v) {}

		float u;
		float v;
	};

	struct Vertex {
		Position position;
		ColorRGBA8 color;
		//UV texture coordinates
		UV_Coordinates uv_coordinates;

		void set_position(float x, float y) {
			position = Position(x, y);
		}

		void set_color(GLubyte r, GLubyte g, GLubyte b, GLubyte a) {
			color = ColorRGBA8(r, g, b, a);
		}

		void set_UV_coordinates(float u, float v) {
			uv_coordinates = UV_Coordinates(u, v);
		}
	};
}
#include "Window.h"
#include"Errors.h"

namespace GameEngine {

	Window::Window()
	{
	}


	Window::~Window()
	{
	}

	int Window::create(std::string window_name, int screen_width, int screen_height, unsigned int current_flags)
	{
		Uint32 _flags = SDL_WINDOW_OPENGL;

		m_screen_width = screen_width;
		m_screen_height = screen_height;

		if (current_flags & Window_Flags::INVISIBLE) {
			_flags |= SDL_WINDOW_HIDDEN;
		}
		if (current_flags & Window_Flags::FULLSCREEN) {
			_flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}
		if (current_flags & Window_Flags::BORDERLESS) {
			_flags |= SDL_WINDOW_BORDERLESS;
		}
		m_SDL_window = SDL_CreateWindow(window_name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen_width, screen_height, _flags);

		if (m_SDL_window == nullptr) {
			fatal_error("SDL Window could not be created!");
		}

		SDL_GLContext _context = SDL_GL_CreateContext(m_SDL_window);

		if (_context == nullptr) {
			fatal_error("SDL_GLContext could not be created!");
		}

		GLenum _error = glewInit();

		if (_error != GLEW_OK) {
			fatal_error("Could not initialize Glew");
		}

		//Check OpenGL version
		std::printf("*** OpenGL Version %s ***\n", glGetString(GL_VERSION));

		//Set the background color
		glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

		//Set VSYNC
		SDL_GL_SetSwapInterval(1);

		//Enable alpha blend
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		return 0;
	}

	void Window::swap_buffer()
	{
		//Swap our buffer and draw everything to the screen
		SDL_GL_SwapWindow(m_SDL_window);
	}
}
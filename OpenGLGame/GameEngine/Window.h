#pragma once

#include<SDL\SDL.h>
#include<GL\glew.h>
#include<string>

namespace GameEngine {
	enum Window_Flags {
		INVISIBLE = 0x1,
		FULLSCREEN = 0x2,
		BORDERLESS = 0x4
	};

	class Window
	{
	public:
		Window();
		~Window();

		int create(std::string window_name, int screen_width, int screen_height, unsigned int current_flags);
		void swap_buffer();
		int get_screen_width() { return m_screen_width; }
		int get_screen_height() { return m_screen_height; }
	private:
		SDL_Window *m_SDL_window;
		int m_screen_width;
		int m_screen_height;
	};
}


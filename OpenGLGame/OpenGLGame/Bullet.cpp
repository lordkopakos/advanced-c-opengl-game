#include "Bullet.h"

#include<GameEngine\GLTexture.h>
#include <GameEngine\ResourceManager.h>

Bullet::Bullet()
{
}


Bullet::Bullet(glm::vec2 position, glm::vec2 direction, float speed, int life_time)
{
	init(position, direction, speed, life_time);
}

Bullet::~Bullet()
{
}

void Bullet::init(glm::vec2 position, glm::vec2 direction, float speed, int life_time)
{
	m_position = position;
	m_direction = direction;
	m_speed = speed;
	m_life_time = life_time;
}

void Bullet::draw(GameEngine::SpriteBatch & sprite_batch)
{
	GameEngine::ColorRGBA8 _color;

	_color.r = 255;
	_color.g = 255;
	_color.b = 255;
	_color.a = 255;

	glm::vec4 _uv(0.0f, 0.0f, 1.0f, 1.0f);
	GameEngine::GLTexture _texture = GameEngine::ResourceManager::get_texture("Textures/PNG/CharacterRight_Jump.png");

	glm::vec4 _position_and_size = glm::vec4(m_position.x, m_position.y, 30, 30);

	sprite_batch.draw(_position_and_size, _uv, _texture.id, 0.0f, _color);
}

bool Bullet::update()
{
	m_position += m_direction*m_speed; 
	m_life_time--;
	if (m_life_time == 0)
		return true;

	return false;
}

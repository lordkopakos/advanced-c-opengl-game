#pragma once

#include<glm/glm.hpp>
#include<GameEngine\SpriteBatch.h>

class Bullet
{
public:
	Bullet();
	Bullet(glm::vec2 position, glm::vec2 direction, float speed, int life_time);
	~Bullet();

	void init(glm::vec2 position, glm::vec2 direction, float speed, int life_time);
	void draw(GameEngine::SpriteBatch& sprite_batch);

	//Returns true when we are out of life
	bool update();

private:
	int m_life_time;
	float m_speed;
	glm::vec2 m_direction;
	glm::vec2 m_position;
};


#include "MainGame.h"

#include<GameEngine\ResourceManager.h>
#include<GameEngine\Errors.h>
#include<GameEngine\SDL_init.h>

#include<iostream>
#include<string>
#include<array>

MainGame::MainGame()
	:m_screen_width(1024),
	m_screen_height(768),
	m_game_state(GameState::PLAY),
	m_time(0.0f),
	m_max_FPS(600.0f)
{
	m_camera_2D.init(m_screen_width, m_screen_height);
}


MainGame::~MainGame()
{
}

void MainGame::run()
{
	init_systems();

	game_loop();
}

void MainGame::init_systems()
{
	GameEngine::init();

	m_window.create("OpenGL Game", m_screen_width, m_screen_height, 0);

	init_shaders();

	m_sprite_batch.init();

	m_FPS_limiter.init(m_max_FPS);
}

void MainGame::init_shaders()
{
	//TODO for all shaders in "Shaders" file.
	m_color_shader_program.compile_shaders("Shaders/colorShading.vert", "Shaders/colorShading.frag");
	m_color_shader_program.add_attribute("vertex_position");
	m_color_shader_program.add_attribute("vertex_color");
	m_color_shader_program.add_attribute("vertex_UV_coordinates");
	m_color_shader_program.link_shaders();
}

void MainGame::game_loop()
{
	while (m_game_state != GameState::EXIT) {
		//Used for frame time measuring
		m_FPS_limiter.begin();

		process_input();
		m_time += 0.01;

		m_camera_2D.update();

		//for (auto &_bullet : m_bullets) {
		//	if (_bullet.update())
		//		_bullet;
		//}

		for(auto i=0; i<m_bullets.size();)
		{
			if (m_bullets[i].update())
			{
				m_bullets[i] = m_bullets.back();
				m_bullets.pop_back();
			}
			else
				i++;
		}

		draw_game();

		m_FPS = m_FPS_limiter.end();

		//print only once every 10 frames
		static int _frame_counter = 0;
		_frame_counter++;
		if (_frame_counter == 1000) {
			std::cout << "FPS: " << m_FPS << std::endl;
			_frame_counter = 0;
		}
	}
}

void MainGame::process_input()
{
	const float CAMERA_SPEED = 2.0f;
	const float SCALE_SPEED = 0.1f;

	SDL_Event _event;
	while (SDL_PollEvent(&_event)) {
		switch (_event.type)
		{
		case SDL_QUIT:
			m_game_state = GameState::EXIT;
			break;
		case SDL_KEYDOWN:
			m_input_manager.press_key(_event.key.keysym.sym);
			break;
		case SDL_KEYUP:
			m_input_manager.release_key(_event.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			m_input_manager.press_key(_event.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			m_input_manager.release_key(_event.button.button);
			break;
		case SDL_MOUSEMOTION:
			m_input_manager.set_mouse_coordinates(_event.motion.x, _event.motion.y);
			break;

		default:
			break;
		}
	}

	if (m_input_manager.is_key_down(SDLK_w))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(0.0, -CAMERA_SPEED));

	if (m_input_manager.is_key_down(SDLK_s))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(0.0, CAMERA_SPEED));

	if (m_input_manager.is_key_down(SDLK_a))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(CAMERA_SPEED, 0.0));

	if (m_input_manager.is_key_down(SDLK_d))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(-CAMERA_SPEED, 0.0));

	if (m_input_manager.is_key_down(SDLK_q))
		m_camera_2D.set_scale(m_camera_2D.get_scale() + SCALE_SPEED);

	if (m_input_manager.is_key_down(SDLK_e))
		m_camera_2D.set_scale(m_camera_2D.get_scale() - SCALE_SPEED);

	if (m_input_manager.is_key_down(SDLK_ESCAPE))
		m_game_state = GameState::EXIT;

	if (m_input_manager.is_key_down(SDL_BUTTON_LEFT))
	{
		glm::vec2 _mouse_coordinates = m_input_manager.get_mouse_coordinates();
		_mouse_coordinates = m_camera_2D.convert_screen_to_world(_mouse_coordinates);
		std::cout << _mouse_coordinates.x << " " << _mouse_coordinates.y << std::endl;

		glm::vec2 _player_position(0.0f);
		glm::vec2 _direction = _mouse_coordinates - _player_position;

		_direction = glm::normalize(_direction);

		m_bullets.emplace_back(_player_position, _direction, 5.0f, 100);
	}

}

void MainGame::draw_game()
{
	//Set the base depth to 1.0
	glClearDepth(1.0);
	//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	m_color_shader_program.use();

	glActiveTexture(GL_TEXTURE0);

	GLint _texture_location = m_color_shader_program.get_uniform_location("my_sampler");
	glUniform1i(_texture_location, 0);

	/*GLuint _time_location = m_color_shader_program.get_uniform_location("time");
	glUniform1f(_time_location, m_time);*/

	//Set the camera matrix
	GLint _matrix_location = m_color_shader_program.get_uniform_location("matrix_position");
	glm::mat4 _camera_matrix = m_camera_2D.get_camera_matrix();
	glUniformMatrix4fv(_matrix_location, 1, GL_FALSE, &_camera_matrix[0][0]);

	m_sprite_batch.begin();

	glm::vec4 _pos(0.0f, 0.0f, 50.0f, 50.0f);
	glm::vec4 _uv(0.0f, 0.0f, 1.0f, 1.0f);
	GameEngine::GLTexture _texture = GameEngine::ResourceManager::get_texture("Textures/PNG/CharacterRight_Jump.png");
	GameEngine::ColorRGBA8 _color;

	_color.r = 255;
	_color.g = 255;
	_color.b = 255;
	_color.a = 255;

	m_sprite_batch.draw(_pos, _uv, _texture.id, 0.0f, _color);

	for (auto &_bullet : m_bullets) {
		_bullet.draw(m_sprite_batch);
	}

	m_sprite_batch.end();

	m_sprite_batch.render_batch();

	glBindTexture(GL_TEXTURE_2D, 0);

	m_color_shader_program.unuse();
	m_window.swap_buffer();
}


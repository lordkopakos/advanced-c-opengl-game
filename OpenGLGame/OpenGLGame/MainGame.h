#pragma once

#include"Bullet.h"
#include<GameEngine\Timinig.h>
#include<GameEngine\Input_Manager.h>
#include<GameEngine\SpriteBatch.h>
#include<GameEngine\Camera2D.h>
#include<GameEngine\Sprite.h>
#include<GameEngine\GLSLProgram.h>
#include<GameEngine\GLTexture.h>
#include<GameEngine\Window.h>

#include<SDL\SDL.h>
#include<GL/glew.h>
#include<vector>


enum class GameState {
	PLAY,
	EXIT
};

class MainGame
{
public:
	MainGame();
	~MainGame();

	void run();

private:
	void init_systems();
	void init_shaders();
	void game_loop();
	void process_input();
	void draw_game();
	
	GameEngine::Window m_window;

	int m_screen_width;
	int m_screen_height;
	GameState m_game_state;

	GameEngine::GLSLProgram m_color_shader_program;

	GameEngine::Camera2D m_camera_2D;

	GameEngine::SpriteBatch m_sprite_batch;

	GameEngine::Input_Manager m_input_manager;

	GameEngine::FPS_Limiter m_FPS_limiter;

	std::vector<Bullet> m_bullets;

	float m_max_FPS;
	float m_FPS;

	float m_time;
};


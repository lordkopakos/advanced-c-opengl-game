#version 130
//The fragment shader operates on each pixel in a given polygon

in vec2 fragment_position;
in vec4 fragment_color;

//This is the 3 component float vector that gets outputted to the screen
//for each pixel.
out vec4 color;

void main() {

    color = fragment_color;
}
#include "Main_Simulation.h"


#include "Main_Simulation.h"

#include<GameEngine\ResourceManager.h>
#include<GameEngine\Errors.h>
#include<GameEngine\SDL_init.h>

#include<iostream>
#include<string>
#include<array>
#include<forward_list>
Main_Simulation::Main_Simulation()
	:m_screen_width(1024),
	m_screen_height(768),
	m_game_state(Game_State::PLAY)//,
	//m_time(0.0f)
{
	m_camera_2D.init(m_screen_width, m_screen_height);
}


Main_Simulation::~Main_Simulation()
{

}

void Main_Simulation::run()
{
	init_systems();

	game_loop();
}

void Main_Simulation::init_systems()
{
	GameEngine::init();

	m_window.create("OpenGL Game", m_screen_width, m_screen_height, 0);

	init_shaders();

	m_sprite_batch.init();
}

void Main_Simulation::init_shaders()
{
	//TODO for all shaders in "Shaders" file.
	m_texture_program.compile_shaders("Shaders/colorShading.vert", "Shaders/colorShading.frag");
	m_texture_program.add_attribute("vertex_position");
	m_texture_program.add_attribute("vertex_color");
	m_texture_program.add_attribute("vertex_UV_coordinates");
	m_texture_program.link_shaders();
}

void Main_Simulation::game_loop()
{
	while (m_game_state != Game_State::EXIT) {

		process_input();

		m_camera_2D.update();

		//m_time += 0.1;

		draw_game();
	}
}

void Main_Simulation::process_input()
{
	const float CAMERA_SPEED = 2.0f;
	const float SCALE_SPEED = 0.1f;

	SDL_Event _event;
	while (SDL_PollEvent(&_event)) {
		switch (_event.type)
		{
		case SDL_QUIT:
			m_game_state = Game_State::EXIT;
			break;
		case SDL_KEYDOWN:
			m_input_manager.press_key(_event.key.keysym.sym);
			break;
		case SDL_KEYUP:
			m_input_manager.release_key(_event.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			m_input_manager.press_key(_event.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			m_input_manager.release_key(_event.button.button);
			break;
		case SDL_MOUSEMOTION:
			m_input_manager.set_mouse_coordinates(_event.motion.x, _event.motion.y);
			break;

		default:
			break;
		}
	}

	if (m_input_manager.is_key_pressed(SDLK_w))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(0.0, -CAMERA_SPEED));

	if (m_input_manager.is_key_pressed(SDLK_s))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(0.0, CAMERA_SPEED));

	if (m_input_manager.is_key_pressed(SDLK_a))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(CAMERA_SPEED, 0.0));

	if (m_input_manager.is_key_pressed(SDLK_d))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(-CAMERA_SPEED, 0.0));

	if (m_input_manager.is_key_pressed(SDLK_q))
		m_camera_2D.set_scale(m_camera_2D.get_scale() + SCALE_SPEED);

	if (m_input_manager.is_key_pressed(SDLK_e))
		m_camera_2D.set_scale(m_camera_2D.get_scale() - SCALE_SPEED);

	if (m_input_manager.is_key_pressed(SDLK_ESCAPE))
		m_game_state = Game_State::EXIT;

	if (m_input_manager.is_key_pressed(SDL_BUTTON_LEFT))
	{
		glm::vec2 _mouse_coordinates = m_input_manager.get_mouse_coordinates();
		_mouse_coordinates = m_camera_2D.convert_screen_to_world(_mouse_coordinates);
		std::cout << _mouse_coordinates.x << " " << _mouse_coordinates.y << std::endl;

		glm::vec2 _player_position(0.0f);
		glm::vec2 _direction = _mouse_coordinates - _player_position;

		_direction = glm::normalize(_direction);
	}

}

void Main_Simulation::draw_game()
{
	//Set the base depth to 1.0
	glClearDepth(1.0);
	//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_texture_program.use();

	glActiveTexture(GL_TEXTURE0);

	GLint _texture_location = m_texture_program.get_uniform_location("my_sampler");
	glUniform1i(_texture_location, 0);

	//GLuint _time_location = m_texture_program.get_uniform_location("time");
	//glUniform1f(_time_location, m_time);

	//Set the camera matrix
	GLint _matrix_location = m_texture_program.get_uniform_location("matrix_position");
	glm::mat4 _camera_matrix = m_camera_2D.get_camera_matrix();
	glUniformMatrix4fv(_matrix_location, 1, GL_FALSE, &_camera_matrix[0][0]);

	m_sprite_batch.begin();

	glm::vec4 _pos(0.0f, 0.0f, 50.0f, 50.0f);
	glm::vec4 _uv(0.0f, 0.0f, 1.0f, 1.0f);
	GameEngine::GLTexture _texture = GameEngine::ResourceManager::get_texture("Textures/blank.png");
	GameEngine::Color _color;

	_color.r = 0;
	_color.g = 255;
	_color.b = 0;
	_color.a = 255;

	m_sprite_batch.draw(_pos, _uv, _texture.id, 0.0f, _color);
	m_sprite_batch.draw(_pos + glm::vec4(51.0f, 0.0f, 0.0f, 0.0f), _uv, _texture.id, 0.0f, _color);

	m_sprite_batch.end();

	m_sprite_batch.render_batch();

	glBindTexture(GL_TEXTURE_2D, 0);

	m_texture_program.unuse();
	m_window.swap_buffer();
}


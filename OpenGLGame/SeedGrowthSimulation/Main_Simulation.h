#pragma once

#include<GameEngine\Timinig.h>
#include<GameEngine\Input_Manager.h>
#include<GameEngine\SpriteBatch.h>
#include<GameEngine\Camera2D.h>
#include<GameEngine\Sprite.h>
#include<GameEngine\GLSLProgram.h>
#include<GameEngine\GLTexture.h>
#include<GameEngine\Window.h>

#include<SDL\SDL.h>
#include<GL/glew.h>
#include<vector>
#include<memory>


enum class Game_State {
	PLAY,
	EXIT
};

class Main_Simulation
{
public:
	Main_Simulation();
	~Main_Simulation();

	void run();

private:
	void init_systems();
	void init_shaders();
	void game_loop();
	void process_input();
	void draw_game();

	int m_screen_width;
	int m_screen_height;
	Game_State m_game_state;

	GameEngine::Window m_window;

	GameEngine::GLSLProgram m_texture_program;

	GameEngine::Camera2D m_camera_2D;

	GameEngine::Input_Manager m_input_manager;

	GameEngine::SpriteBatch m_sprite_batch;

	//float m_time;
};

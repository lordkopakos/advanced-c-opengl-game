#pragma once

#include"Cell.h"

#include<vector>
class Mesh
{

public:
	Mesh();
	~Mesh();

private:
	std::vector<Cell> m_cells;

};


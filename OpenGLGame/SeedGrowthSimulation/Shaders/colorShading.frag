#version 130

in vec2 fragment_position;

in vec4 fragment_color;

in vec2 fragment_UV_coordinates;

out vec4 color;

//uniform float time;
uniform sampler2D my_sampler;

void main(){

	vec4 texture_color = texture(my_sampler, fragment_UV_coordinates);

	//Simple using texture
	color = fragment_color* texture_color;

}
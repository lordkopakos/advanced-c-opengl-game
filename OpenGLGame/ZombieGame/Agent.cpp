#include "Agent.h"

#include <GameEngine\ResourceManager.h>

#include "Level.h"

Agent::Agent()
{
}


Agent::~Agent()
{
}

bool Agent::collide_with_level(const std::vector<std::string>& level_data)
{
	std::vector<glm::vec2> _collide_tile_positions;
	//First corner
	check_tile_position(level_data, _collide_tile_positions, m_position.x, m_position.y);
	//Second corner
	check_tile_position(level_data, _collide_tile_positions, m_position.x + AGENT_WIDTH, m_position.y);
	//Third corner
	check_tile_position(level_data, _collide_tile_positions, m_position.x, m_position.y + AGENT_WIDTH);
	//Fourth corner
	check_tile_position(level_data, _collide_tile_positions, m_position.x + AGENT_WIDTH, m_position.y + AGENT_WIDTH);

	if (_collide_tile_positions.size() == 0)
		return false;

	for (auto& _collide_tile_position : _collide_tile_positions) 
		collide_with_tile(_collide_tile_position);

	return true;
}

bool Agent::collide_with_agent(std::shared_ptr<Agent> agent)
{
	const float MIN_DISTANCE = AGENT_RADIUS * 2.0f;

	glm::vec2 center_position_A = m_position + glm::vec2(AGENT_RADIUS);
	glm::vec2 center_position_B = agent->get_position() + glm::vec2(AGENT_RADIUS);

	glm::vec2 distance_vec = center_position_A - center_position_B;

	float distance = glm::length(distance_vec);
	float collision_depth = MIN_DISTANCE - distance;

	if (collision_depth > 0)
	{
		glm::vec2 collision_depth_vec = glm::normalize(distance_vec)*collision_depth;

		m_position += collision_depth_vec / 2.0f;
		agent->m_position -= collision_depth_vec / 2.0f;
		return true;
	}

	return false;
}

bool Agent::apply_damege(float damage)
{
	m_health -= damage;
	if (m_health <= 0)
		return true;
	return false;
}

void Agent::check_tile_position(const std::vector<std::string>& level_data, std::vector<glm::vec2>& collide_tile_positions, float x, float y)
{
	//Check the four corners
	glm::vec2 _corner_position = glm::vec2(floor(x / (float)TILE_WIDTH),
										   floor(y / (float)TILE_WIDTH));

	if (_corner_position.x < 0 || _corner_position.x >= level_data[0].length() ||
		_corner_position.y < 0 || _corner_position.y >= level_data.size())
		return;
	if (level_data[_corner_position.y][_corner_position.x] != '.')
		collide_tile_positions.push_back(_corner_position*(float)TILE_WIDTH + glm::vec2((float)TILE_WIDTH / 2.0f));
}

//AABB collision
void Agent::collide_with_tile(glm::vec2 tile_position)
{
	const float TILE_RADIUS = (float)TILE_WIDTH / 2.0f;
	const float MIN_DISTANCE = AGENT_RADIUS + TILE_RADIUS;

	glm::vec2 _center_player_position = m_position + glm::vec2(AGENT_RADIUS);
	glm::vec2 _distance_vec = _center_player_position - tile_position;
	
	float _depth_x = MIN_DISTANCE - abs(_distance_vec.x);
	float _depth_y = MIN_DISTANCE - abs(_distance_vec.y);

	//If is true we are colliding
	if (_depth_x > 0 && _depth_y > 0)
	{
		if (std::max(_depth_x, 0.0f) < std::max(_depth_y, 0.0f))
		{
			if(_distance_vec.x < 0)
				m_position.x -= _depth_x;
			else
				m_position.x += _depth_x;
		}
		else
		{
			if (_distance_vec.y < 0)
				m_position.y -= _depth_y;
			else
				m_position.y += _depth_y;
		}
	}
}

void Agent::draw(GameEngine::SpriteBatch & sprite_batch)
{
	const glm::vec4 UV_rect(0.0f, 0.0f, 1.0f, 1.0f);

	glm::vec4 dest_rect;
	dest_rect.x = m_position.x;
	dest_rect.y = m_position.y;
	dest_rect.z = AGENT_WIDTH;
	dest_rect.w = AGENT_WIDTH;

	sprite_batch.draw(dest_rect, UV_rect, m_texture_ID, 0.0f, m_color/*, m_direction*/);
}


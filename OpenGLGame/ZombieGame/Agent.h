#pragma once
#include<glm\glm.hpp>
#include <GameEngine\SpriteBatch.h>

constexpr float AGENT_WIDTH = 60;
constexpr float AGENT_RADIUS = AGENT_WIDTH / 2.0f;

class Zombie;
class Human;

class Agent
{
public:
	Agent();
	virtual ~Agent();

	virtual void update(const std::vector<std::string>& level_data,
						std::vector<std::shared_ptr<Human>>& humans,
						std::vector<std::shared_ptr<Zombie>>& zombies,
						float delta_time) = 0;

	bool collide_with_level(const std::vector<std::string>& level_data);

	bool collide_with_agent(std::shared_ptr<Agent> agent);

	bool apply_damege(float damage);

	void draw(GameEngine::SpriteBatch & sprite_batch);

	glm::vec2 get_position() const { return m_position; }

protected:
	void check_tile_position(const std::vector<std::string>& level_data,
							 std::vector<glm::vec2>& collide_tile_positions,
							 float x,
							 float y);

	void collide_with_tile(glm::vec2 tile_position);

	glm::vec2 m_position;
	glm::vec2 m_direction = glm::vec2(1.0f, 0.0f);
	GameEngine::ColorRGBA8 m_color;
	float m_speed;
	float m_health;
	GLuint m_texture_ID;
};


#include"Bullet.h"

#include"Agent.h"
#include"Human.h"
#include"Level.h"

#include<GameEngine\ResourceManager.h>

Bullet::Bullet(glm::vec2 position, glm::vec2 direction, float damage, float speed)
	:m_position(position), m_direction(direction), m_damage(damage), m_speed(speed)
{
}

bool Bullet::update(const std::vector<std::string>& level_data, float delta_time)
{
	m_position += m_direction*m_speed*delta_time;
	return collide_with_world(level_data);
}

void Bullet::draw(GameEngine::SpriteBatch& sprite_batch)
{
	glm::vec4 _dest_rect(m_position.x+BULLET_RADIUS, m_position.y+BULLET_RADIUS, BULLET_RADIUS*2, BULLET_RADIUS*2);
	glm::vec4 _UV_rect(0.0f, 0.0f, 1.0f, 1.0f);
	GameEngine::ColorRGBA8 _color;
	_color.r = 75;
	_color.g = 75;
	_color.b = 75;
	_color.a = 255;
	sprite_batch.draw(_dest_rect, _UV_rect, GameEngine::ResourceManager::get_texture("Textures/circle.png").id, 0.0f, _color);
}

bool Bullet::collide_with_agent(std::shared_ptr<Agent> agent)
{
	const float MIN_DISTANCE = AGENT_RADIUS + BULLET_RADIUS;

	glm::vec2 center_position_A = m_position;
	glm::vec2 center_position_B = agent->get_position() + glm::vec2(AGENT_RADIUS);

	glm::vec2 distance_vec = center_position_A - center_position_B;

	float distance = glm::length(distance_vec);
	float collision_depth = MIN_DISTANCE - distance;

	if (collision_depth > 0)
		return true;

	return false;
	return false;
}

float Bullet::get_damage() const
{
	return m_damage;
}

bool Bullet::collide_with_world(const std::vector<std::string>& level_data)
{
	glm::ivec2 grid_position;

	grid_position.x =floor( m_position.x / (float)TILE_WIDTH);
	grid_position.y = floor(m_position.y / (float)TILE_WIDTH);

	if (grid_position.x < 0 || grid_position.x >= level_data[0].length() ||
		grid_position.y < 0 || grid_position.y >= level_data.size())
		return true;

	return level_data[grid_position.y][grid_position.x] != '.';
}

#pragma once

#include<GameEngine\SpriteBatch.h>

#include<glm/glm.hpp>

#include<vector>
#include<memory>

class Agent;
class Human;
class Zombie;

constexpr int BULLET_RADIUS = 5;

class Bullet 
{
public: 
	Bullet(glm::vec2 position, glm::vec2 direction, float damage, float speed);

	bool update(const std::vector<std::string>& level_data, float delta_time);

	void draw(GameEngine::SpriteBatch& sprite_batch);

	bool collide_with_agent(std::shared_ptr<Agent> agent);

	float get_damage() const;

	glm::vec2 get_position() const { return m_position; }
private:
	bool collide_with_world(const std::vector<std::string>& level_data);

	glm::vec2 m_position;
	glm::vec2 m_direction;
	float m_damage;
	float m_speed;
};
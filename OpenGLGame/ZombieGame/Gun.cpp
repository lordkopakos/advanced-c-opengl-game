#include "Gun.h"

#include<random>
#include<ctime>

#include<glm\gtx\rotate_vector.hpp>
Gun::Gun(const std::string & name, int fire_rate, int bullet_per_shot, float spread, float bullet_damage, float bullet_speed, GameEngine::Sound_effect fire_effect)
: m_name(name), m_fire_rate(fire_rate),m_bullets_per_shot(bullet_per_shot), m_spread(spread), 
m_bullet_damage(bullet_damage), m_bullet_speed(bullet_speed), m_frame_counter(0), m_fire_effect(fire_effect)
{
}

Gun::~Gun()
{
}

void Gun::update(bool is_mouse_down, const glm::vec2& position, const glm::vec2& direction, std::vector<Bullet>& bullets, float delta_time)
{
	m_frame_counter+=1.0f+delta_time;
	if (is_mouse_down)
		if (m_frame_counter >= m_fire_rate)
		{
			fire(direction, position, bullets);
			m_frame_counter = 0;
		}
}

void Gun::fire(const glm::vec2& direction, const glm::vec2& position, std::vector<Bullet>& bullets)
{
	static std::mt19937 _random_engine(time(nullptr));
	std::uniform_real_distribution<float> _rand_rotate(-m_spread, m_spread);

	m_fire_effect.play();

	for (int i = 0; i < m_bullets_per_shot; i++)
		bullets.emplace_back(position, 
							 glm::rotate(direction, _rand_rotate(_random_engine)), 
							 m_bullet_damage, 
							 m_bullet_speed);
	

}

#pragma once
#include"Bullet.h"

#include<GameEngine\AudioEngine.h>

#include<glm\glm.hpp>

#include<vector>
#include<string>
class Gun
{
public:
	Gun(const std::string& name, int fire_rate, int bullet_per_shot, float spread, float bullet_damage, float bullet_speed, GameEngine::Sound_effect fire_effect);
	~Gun();

	void update(bool is_mouse_down,const glm::vec2& position, const glm::vec2& direction, std::vector<Bullet>& bullets, float delta_time);
	
private:
	void fire(const glm::vec2& direction, const glm::vec2& position, std::vector<Bullet>& bullets);

	GameEngine::Sound_effect m_fire_effect;

	std::string m_name;
	int m_fire_rate;
	int m_bullets_per_shot;
	float m_spread;

	float m_bullet_speed;
	float m_bullet_damage;

	int m_frame_counter;

};


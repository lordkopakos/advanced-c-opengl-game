#include "Human.h"

#include<GameEngine\ResourceManager.h>

#include<glm\gtx\rotate_vector.hpp>

#include <random>
#include <ctime>

Human::Human() :m_frames(0)
{
}

Human::~Human()
{
}

void Human::init(float speed, glm::vec2 position)
{
	static std::mt19937 _random_engine(time(nullptr));
	static std::uniform_real_distribution<float> _rand_direction(-1.0f, 1.0f);

	m_health = 20;

	m_color = GameEngine::ColorRGBA8(255, 255, 255, 255);

	m_speed = speed;
	m_position = position;
	m_direction = glm::vec2(_rand_direction(_random_engine), _rand_direction(_random_engine));
	if (m_direction.length() == 0)
		m_direction = glm::vec2(1.0f, 0.0f);

	m_direction = glm::normalize(m_direction);

	m_texture_ID = GameEngine::ResourceManager::get_texture("Character/Human/human.png").id;
}

void Human::update(const std::vector<std::string>& level_data,
					std::vector<std::shared_ptr<Human>>& humans,
					std::vector<std::shared_ptr<Zombie>>& zombies,
					float delta_time)
{
	static std::mt19937 _random_engine(time(nullptr));
	static std::uniform_real_distribution<float> _rand_rotate(-2.0f, 2.0f);

	m_position += m_direction*m_speed * delta_time;

	if (m_frames == 20)
	{
		m_direction = glm::rotate(m_direction, _rand_rotate(_random_engine));
		m_frames = 0;
	}
	else
		m_frames++;

	if (collide_with_level(level_data)) 
		m_direction = glm::rotate(m_direction, _rand_rotate(_random_engine));
}

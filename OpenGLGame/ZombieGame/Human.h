#pragma once

#include "Agent.h"

class Human :public Agent
{
public:
	Human();
	virtual ~Human();

	void init(float speed, glm::vec2 position);

	virtual void update(const std::vector<std::string>& level_data,
						std::vector<std::shared_ptr<Human>>& humans,
						std::vector<std::shared_ptr<Zombie>>& zombies,
						float delta_time) override;

private: 
	int m_frames;
};


#include "Level.h"

#include<GameEngine\Errors.h>

#include<GameEngine\ResourceManager.h>

#include<fstream>
#include<iostream>

Level::Level()
{
}


Level::Level(const std::string & file_name)
{

	std::ifstream _file{ file_name };
	if (_file.fail())
		GameEngine::fatal_error("Faile to open: " + file_name);

	std::string _tmp;
	_file >> _tmp >> m_number_of_humans;

	std::getline(_file, _tmp); //Throw away first line

	while (std::getline(_file, _tmp)) 
		m_level_data.push_back(_tmp);

	m_title_map.init();
	m_title_map.begin();

	glm::vec4 UV_rectangle = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	GameEngine::ColorRGBA8 _white_color;
	
	_white_color.r = 255;
	_white_color.g = 255;
	_white_color.b = 255;
	_white_color.a = 255;

	int y = 0;

	//Render all the titles
	for (auto &titles_line : m_level_data) 
	{
		int x = 0;
		for (auto &title : titles_line)
		{
			//Get destination rectangle
			glm::vec4 destination_rectangle(x*TILE_WIDTH, y*TILE_WIDTH, TILE_WIDTH, TILE_WIDTH);
			switch (title) 
			{
			case 'B':
			case 'R':
				m_title_map.draw(destination_rectangle, 
								 UV_rectangle, 
								 GameEngine::ResourceManager::get_texture("Textures/red_bricks.png").id,
								 0.0f,
								 _white_color);
				break;

			case 'G':
				m_title_map.draw(destination_rectangle,
								 UV_rectangle,
								 GameEngine::ResourceManager::get_texture("Textures/glass.png").id,
								 0.0f,
								 _white_color);
				break;

			case 'L':
				m_title_map.draw(destination_rectangle,
								 UV_rectangle,
								 GameEngine::ResourceManager::get_texture("Textures/light_bricks.png").id,
								 0.0f,
								 _white_color);
				break;

			case '.':
				break;

			case '@':
				titles_line[x] = '.'; //Dont collide with @
				m_start_player_position.x = x*TILE_WIDTH;
				m_start_player_position.y = y*TILE_WIDTH;
				break;
			case 'Z':
				titles_line[x] = '.'; //Dont collide with Z
				m_zombie_positions.emplace_back(x*TILE_WIDTH, y*TILE_WIDTH);
				break;
			default:
				std::printf("Unexpected symbol %c at (%d,%d)", title, x, y);
			}
			x++;
		}
		y++;
	}

	m_title_map.end();
	_file.close();
}

Level::~Level()
{
}

void Level::draw()
{
	m_title_map.render_batch();
}

#pragma once

#include<GameEngine\SpriteBatch.h>

#include<string>
#include<vector>

const int TILE_WIDTH = 64;

class Level
{
public:
	//Load a level
	Level();
	Level(const std::string& file_name);
	~Level();

	void draw();

	//Getters
	int get_width() const { return m_level_data[0].size(); }
	int get_hight() const { return m_level_data.size(); }
	int get_number_of_humans() const { return m_number_of_humans; }
	const std::vector<std::string>& get_level_data() const { return m_level_data; }
	glm::vec2 get_start_player_position() const { return m_start_player_position; }
	const std::vector<glm::vec2>& get_start_zombie_positions() const{ return m_zombie_positions; }

private:
	std::vector<std::string> m_level_data;
	int m_number_of_humans;
	GameEngine::SpriteBatch m_title_map;

	glm::vec2 m_start_player_position;
	std::vector<glm::vec2> m_zombie_positions;
};


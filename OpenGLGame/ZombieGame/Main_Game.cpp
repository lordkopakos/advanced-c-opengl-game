#include "Main_Game.h"

#include"Zombie.h"
#include"Gun.h"

#include<GameEngine\Timinig.h>
#include<GameEngine\ResourceManager.h>
#include<GameEngine\Errors.h>
#include<GameEngine\SDL_init.h>
#include<GameEngine\Errors.h>
#include<GameEngine\ResourceManager.h>

#include<glm\gtx\rotate_vector.hpp>
#include<Box2D\Box2D.h>
#include<GameEngine\GUI.h>
#include<iostream>
#include<string>
#include<array>
#include<forward_list>
#include<random>
#include<ctime>

constexpr float HUMAN_SPEED = 1.0f;
constexpr float ZOMBIE_SPEED = 1.3f;
constexpr float PLAYER_SPEED = 10.0f;

Main_Game::Main_Game()
	:m_screen_width(1024),
	m_screen_height(768),
	m_game_state(Game_State::PLAY),
	m_FPS(0),
	m_current_level(0),
	m_number_human_killed(0),
	m_number_zombie_killed(0),
	m_player(nullptr)
{
	m_camera_2D.init(m_screen_width, m_screen_height);
}


Main_Game::~Main_Game()
{
	
}

void Main_Game::run()
{
	init_systems();
	init_level();
	
	GameEngine::Music _music = m_audio_engine.load_music("Sound/zombieBackGroundSound.ogg");
	_music.play(-1);

	game_loop();
}

void Main_Game::init_systems()
{
	GameEngine::init();

	//Initialize sound, must happen after GameEngine::init()
	m_audio_engine.init();

	m_window.create("OpenGL Game", m_screen_width, m_screen_height, 0);
	glClearColor(0.7f, 0.7f,0.7f,1.0f);
	init_shaders();

	m_agent_sprite_batch.init();
	m_HUD_sprite_batch.init();

	m_sprite_font = std::make_unique<GameEngine::SpriteFont>(std::string("Fonts/chintzy.ttf").c_str(), 64);

	m_camera_2D.init(m_screen_width, m_screen_height);
	m_HUD_camera.init(m_screen_width, m_screen_height);
	m_HUD_camera.set_position(glm::vec2(m_screen_width / 2, m_screen_height/2));

	//Initialize particles
	m_blood_particle_batch_2D = std::make_shared<GameEngine::Particle_batch_2D>();

	m_blood_particle_batch_2D->init(1000, 0.07f, GameEngine::ResourceManager::get_texture("Textures/circle.png"), 
	[](GameEngine::Particle2D& particle, float delta_time) 
	{
		particle.position += particle.velocity*delta_time;
		particle.color.a = (GLubyte)(particle.life*255.0f);
	});
	
	m_particle_engine_2D.add_particle_batch(m_blood_particle_batch_2D);
}

void Main_Game::init_level()
{
	m_levels.push_back(std::make_unique<Level>("Levels/level1.txt"));
	m_current_level = 0;

	m_player = std::make_shared<Player>();
	m_player->init(PLAYER_SPEED, m_levels[m_current_level]->get_start_player_position(), &m_input_manager, &m_camera_2D, &m_bullets);

	m_humans.push_back(m_player);

	std::mt19937 _random_engine(time(nullptr));

	std::uniform_int_distribution<int> _rand_X(1, m_levels[m_current_level]->get_width()-2);
	std::uniform_int_distribution<int> _rand_Y(1, m_levels[m_current_level]->get_hight()-2);

	for (auto i = 0; i < m_levels[m_current_level]->get_number_of_humans(); i++)
	{
		m_humans.push_back(std::make_shared<Human>());
		glm::vec2 _pos(_rand_X(_random_engine)*TILE_WIDTH, _rand_Y(_random_engine)*TILE_WIDTH);
		m_humans.back()->init(HUMAN_SPEED, _pos);
	}

	const std::vector<glm::vec2>& zombie_positions = m_levels[m_current_level]->get_start_zombie_positions();

	for (auto i = 0; i < zombie_positions.size(); i++)
	{
		m_zombies.push_back(std::make_shared<Zombie>());
		m_zombies.back()->init(ZOMBIE_SPEED, zombie_positions[i]);
	}

	constexpr float BULLET_SPEED = 20.0f;

	m_player->add_gun(std::make_shared<Gun>("Magnum", 30, 1, 0.05f, 30, BULLET_SPEED, m_audio_engine.load_sound_effect("Sound/Gun/magnum.wav")));
	m_player->add_gun(std::make_shared<Gun>("Shutgun", 60, 20, 0.2f, 8, BULLET_SPEED, m_audio_engine.load_sound_effect("Sound/Gun/shotgun.wav")));
	m_player->add_gun(std::make_shared<Gun>("MP5", 5, 1, 0.1f, 15, BULLET_SPEED, m_audio_engine.load_sound_effect("Sound/Gun/mp5.wav")));
}


void Main_Game::init_shaders()
{
	//TODO for all shaders in "Shaders" file.
	m_texture_program.compile_shaders("Shaders/colorShading.vert", "Shaders/colorShading.frag");
	m_texture_program.add_attribute("vertex_position");
	m_texture_program.add_attribute("vertex_color");
	m_texture_program.add_attribute("vertex_UV_coordinates");
	m_texture_program.link_shaders();
}

void Main_Game::game_loop()
{
	constexpr float DESIRED_FPS = 60.0f;
	constexpr int MAX_PHYSICS_STEPS = 6;

	GameEngine::FPS_Limiter FPS_limiter;
	FPS_limiter.set_max_FPS(DESIRED_FPS);

	constexpr float CAMERA_SCALE = 1.0f / 4.0f;
	m_camera_2D.set_scale(CAMERA_SCALE);

	
	constexpr float MS_PER_SECOND = 1000.0f;
	constexpr float DESIRED_FRAMETIME=MS_PER_SECOND/ DESIRED_FPS;
	constexpr float MAX_DELTA_TIME = 1.0f;

	float previous_ticks = SDL_GetTicks();

	while (m_game_state != Game_State::EXIT) {
		FPS_limiter.begin();

		float new_ticks = SDL_GetTicks();
		float frame_time = SDL_GetTicks() - previous_ticks;
		previous_ticks = new_ticks;
		float total_delta_time = frame_time / DESIRED_FRAMETIME;


		check_victory();

		process_input();

		int i = 0;
		while (total_delta_time > 0.0f && i< MAX_PHYSICS_STEPS)
		{
			float delta_time = std::min<float>(total_delta_time, MAX_DELTA_TIME);
			update_agents(delta_time);
			update_bullets(delta_time);
			m_particle_engine_2D.update(delta_time);
			total_delta_time -= delta_time;
			i++;
		}



		m_camera_2D.set_position(m_player->get_position());
		m_camera_2D.update();

		m_HUD_camera.update();

		draw_game();

		m_FPS = FPS_limiter.end();
	}
}

void Main_Game::check_victory()
{
	//TODO: Support for multiple levels
	//m_current_level++; init_level(...);

	if (m_zombies.empty())
	{
		std::printf("*** You win! ***\n You killed %d humans and %d zombies.\n There are %d/%d humans remaining!\n",
			m_number_human_killed,
			m_number_zombie_killed,
			m_humans.size() - 1,
			m_levels[m_current_level]->get_number_of_humans());

		GameEngine::fatal_error("");
	}
}

void Main_Game::update_agents(float delta_time)
{
	for (const auto& human : m_humans) 
		human->update(m_levels[m_current_level]->get_level_data(), m_humans, m_zombies, delta_time);

	for (const auto& zombie : m_zombies)
		zombie->update(m_levels[m_current_level]->get_level_data(), m_humans, m_zombies, delta_time);

	//Update Zombie collisions
	for (auto i = 0; i < m_zombies.size(); i++)
	{
		for (auto j = i + 1; j < m_zombies.size(); j++)
			m_zombies[i]->collide_with_agent(m_zombies[j]);

		for (auto j = 1; j < m_humans.size(); j++)
			if (m_zombies[i]->collide_with_agent(m_humans[j]))
			{
				m_zombies.push_back(std::make_shared<Zombie>());
				m_zombies.back()->init(ZOMBIE_SPEED, m_humans[j]->get_position());
				m_humans.erase(m_humans.begin() + j);
			}
		if (m_zombies[i]->collide_with_agent(m_player))
			GameEngine::fatal_error("YOU LOSE");
		

	}
	//Update Human collisions
	for (auto i = 0; i < m_humans.size(); i++)
		for (auto j = i + 1; j < m_humans.size(); j++)
			m_humans[i]->collide_with_agent(m_humans[j]);
}

void Main_Game::update_bullets(float delta_time)
{
	for (auto i = 0; i < m_bullets.size();)
		if (m_bullets[i].update(m_levels[m_current_level]->get_level_data(), delta_time))
		{
			m_bullets[i] = m_bullets.back();
			m_bullets.pop_back();
		}
		else
			i++;

	bool _bullet_removed = false;

	for (auto i = 0; i < m_bullets.size(); i++)
	{
		_bullet_removed = false;
		for (auto j = 0; j < m_zombies.size();)
			if (m_bullets[i].collide_with_agent(m_zombies[j]))
			{
				add_blood(m_bullets[i].get_position(), 5);

				if (m_zombies[j]->apply_damege(m_bullets[i].get_damage()))
				{
					m_zombies.erase(m_zombies.begin() + j);
					m_number_zombie_killed++;
				}
				else
					j++;

				m_bullets.erase(m_bullets.begin() + i);
				_bullet_removed = true;
				i--;

				break;
			}
			else
				j++;

		if (_bullet_removed == false)
			for (auto j = 1; j < m_humans.size();)
				if (m_bullets[i].collide_with_agent(m_humans[j]))
				{
					add_blood(m_bullets[i].get_position(), 5);
					if (m_humans[j]->apply_damege(m_bullets[i].get_damage()))
					{
						m_humans.erase(m_humans.begin() + j);
						m_number_human_killed++;
					}
					else
						j++;

					m_bullets.erase(m_bullets.begin() + i);
					_bullet_removed = true;
					i--;

					break;
				}
				else
					j++;
	}

	
}

void Main_Game::process_input()
{
	const float CAMERA_SPEED = 2.0f;
	const float SCALE_SPEED = 0.1f;

	SDL_Event _event;
	while (SDL_PollEvent(&_event)) {
		switch (_event.type)
		{
		case SDL_QUIT:
			m_game_state = Game_State::EXIT;
			break;
		case SDL_KEYDOWN:
			m_input_manager.press_key(_event.key.keysym.sym);
			break;
		case SDL_KEYUP:
			m_input_manager.release_key(_event.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			m_input_manager.press_key(_event.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			m_input_manager.release_key(_event.button.button);
			break;
		case SDL_MOUSEMOTION:
			m_input_manager.set_mouse_coordinates(_event.motion.x, _event.motion.y);
			break;

		default:
			break;
		}
	}

	if (m_input_manager.is_key_down(SDLK_w))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(0.0, -CAMERA_SPEED));

	if (m_input_manager.is_key_down(SDLK_s))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(0.0, CAMERA_SPEED));

	if (m_input_manager.is_key_down(SDLK_a))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(CAMERA_SPEED, 0.0));

	if (m_input_manager.is_key_down(SDLK_d))
		m_camera_2D.set_position(m_camera_2D.get_position() + glm::vec2(-CAMERA_SPEED, 0.0));

	if (m_input_manager.is_key_down(SDLK_q))
		m_camera_2D.set_scale(m_camera_2D.get_scale() + SCALE_SPEED);

	if (m_input_manager.is_key_down(SDLK_e))
		m_camera_2D.set_scale(m_camera_2D.get_scale() - SCALE_SPEED);

	if (m_input_manager.is_key_down(SDLK_ESCAPE))
		m_game_state = Game_State::EXIT;

	if (m_input_manager.is_key_down(SDL_BUTTON_LEFT))
	{
		glm::vec2 _mouse_coordinates = m_input_manager.get_mouse_coordinates();
		_mouse_coordinates = m_camera_2D.convert_screen_to_world(_mouse_coordinates);
		std::cout << _mouse_coordinates.x << " " << _mouse_coordinates.y << std::endl;

		glm::vec2 _player_position(0.0f);
		glm::vec2 _direction = _mouse_coordinates - _player_position;

		_direction = glm::normalize(_direction);
	}

}

void Main_Game::draw_HUD()
{
	char buffer[256];

	//Set the camera matrix
	GLint _matrix_location = m_texture_program.get_uniform_location("matrix_position");
	glm::mat4 _camera_matrix = m_HUD_camera.get_camera_matrix();
	glUniformMatrix4fv(_matrix_location, 1, GL_FALSE, &_camera_matrix[0][0]);

	m_HUD_sprite_batch.begin();
	sprintf_s(buffer, "Number of humans: %d", m_humans.size());

	m_sprite_font->draw(m_HUD_sprite_batch, buffer, glm::vec2(0, 0),
		glm::vec2(0.5), 0.0f, GameEngine::ColorRGBA8(255, 255, 255, 255) );

	sprintf_s(buffer, "Number of zombies: %d", m_zombies.size());

	m_sprite_font->draw(m_HUD_sprite_batch, buffer, glm::vec2(0, 36),
		glm::vec2(0.5), 0.0f, GameEngine::ColorRGBA8(255, 255, 255, 255));

	m_HUD_sprite_batch.end();
	m_HUD_sprite_batch.render_batch();
}

void Main_Game::draw_game()
{
	//Set the base depth to 1.0
	glClearDepth(1.0);
	//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_texture_program.use();

	glActiveTexture(GL_TEXTURE0);

	GLint _texture_location = m_texture_program.get_uniform_location("my_sampler");
	glUniform1i(_texture_location, 0);

	/*GLuint _time_location = m_color_shader_program.get_uniform_location("time");
	glUniform1f(_time_location, m_time);*/

	//Set the camera matrix
	GLint _matrix_location = m_texture_program.get_uniform_location("matrix_position");
	glm::mat4 _camera_matrix = m_camera_2D.get_camera_matrix();
	glUniformMatrix4fv(_matrix_location, 1, GL_FALSE, &_camera_matrix[0][0]);
	
	m_levels[m_current_level]->draw();

	m_agent_sprite_batch.begin();

	const glm::vec2 agent_dims(AGENT_RADIUS*2.0f);

	for (const auto & human : m_humans)
		if(m_camera_2D.is_box_in_view(human->get_position(), agent_dims))
			human->draw(m_agent_sprite_batch);

	for (const auto & zombie : m_zombies)
		if (m_camera_2D.is_box_in_view(zombie->get_position(), agent_dims))
			zombie->draw(m_agent_sprite_batch);

	for (auto & bullet : m_bullets)
		bullet.draw(m_agent_sprite_batch);

	m_agent_sprite_batch.end();
	m_agent_sprite_batch.render_batch();
	
	m_particle_engine_2D.draw(m_agent_sprite_batch);

	draw_HUD();

	m_texture_program.unuse();
	m_window.swap_buffer();
}

void Main_Game::add_blood(const glm::vec2 & position, int number_of_particles)
{
	static std::mt19937 _random_engine(time(nullptr));
	static std::uniform_real_distribution<float> _random_angle(0.0f, 360.0f);
	
	glm::vec2 _velocity{2.0f, 0.0f};
	GameEngine::ColorRGBA8 _color(255, 0, 0, 255);
	float _width = 40.0f;

	for (int i = 0; i < number_of_particles; i++)
		m_blood_particle_batch_2D->add_particle(position, glm::rotate(_velocity, _random_angle(_random_engine)), _color, _width);
}




#pragma once

#include"Level.h"
#include"Player.h"
#include"Bullet.h"

#include<GameEngine\Input_Manager.h>
#include<GameEngine\SpriteBatch.h>
#include<GameEngine\Camera2D.h>
#include<GameEngine\Sprite.h>
#include<GameEngine\GLSLProgram.h>
#include<GameEngine\GLTexture.h>
#include<GameEngine\Window.h>
#include<GameEngine\SpriteFont.h>
#include<GameEngine\AudioEngine.h>
#include<GameEngine\Particle_engine_2D.h>
#include<GameEngine\Particle_batch_2D.h>

#include<SDL/SDL.h>
#include<GL/glew.h>
#include<vector>
#include<memory>

class Zombie;

enum class Game_State {
	PLAY,
	EXIT
};

class Main_Game
{
public:
	Main_Game();
	~Main_Game();

	void run();

private:
	void init_systems();
	void init_shaders();
	void game_loop();
	void check_victory();
	void update_agents(float delta_time);
	void update_bullets(float delta_time);
	void process_input();
	void init_level();
	void draw_HUD();
	void draw_game();
	void add_blood(const glm::vec2& position, int number_of_particles);

	int m_screen_width;
	int m_screen_height;
	int m_FPS;
	int m_current_level;
	int m_number_human_killed;
	int m_number_zombie_killed;

	Game_State m_game_state;

	GameEngine::Window m_window;

	GameEngine::GLSLProgram m_texture_program;

	GameEngine::Camera2D m_camera_2D;
	GameEngine::Camera2D m_HUD_camera;

	GameEngine::Input_Manager m_input_manager;

	GameEngine::SpriteBatch m_agent_sprite_batch;
	GameEngine::SpriteBatch m_HUD_sprite_batch;

	GameEngine::AudioEngine m_audio_engine;

	GameEngine::Particle_engine_2D m_particle_engine_2D;
	std::shared_ptr<GameEngine::Particle_batch_2D> m_blood_particle_batch_2D;

	std::unique_ptr<GameEngine::SpriteFont> m_sprite_font;

	std::vector<std::unique_ptr<Level>> m_levels;
	std::vector<std::shared_ptr<Human>> m_humans;
	std::vector<std::shared_ptr<Zombie>> m_zombies;
	std::vector<Bullet> m_bullets;



	std::shared_ptr<Player> m_player;

};

#include "Player.h"

#include<GameEngine\ResourceManager.h>

#include<SDL\SDL.h>


Player::Player() : m_current_gun_index(-1)
{
}


Player::~Player()
{
}

void Player::init(int speed, glm::vec2 position, GameEngine::Input_Manager * input_manager, GameEngine::Camera2D* camera, std::vector<Bullet>* bullets)
{
	m_input_manager = std::shared_ptr<GameEngine::Input_Manager>(input_manager);
	m_camera = std::shared_ptr<GameEngine::Camera2D>(camera);
	m_bullets = std::unique_ptr<std::vector<Bullet>>(bullets);
	m_speed = speed;
	m_position = position;
	
	m_health = 150;

	m_color = GameEngine::ColorRGBA8(255, 255, 255, 255);

	m_texture_ID = GameEngine::ResourceManager::get_texture("Character/Player/player.png").id;
}

void Player::add_gun(std::shared_ptr<Gun> gun)
{
	m_guns.push_back(gun);
	if (m_current_gun_index == -1)
		m_current_gun_index = 0;
}

void Player::update(const std::vector<std::string>& level_data,
					std::vector<std::shared_ptr<Human>>& humans,
					std::vector<std::shared_ptr<Zombie>>& zombies,
					float delta_time)
{
	if (m_input_manager->is_key_down(SDLK_w))
		m_position.y += m_speed*delta_time;
	else if (m_input_manager->is_key_down(SDLK_s))
		m_position.y -= m_speed*delta_time;

	if (m_input_manager->is_key_down(SDLK_d))
		m_position.x += m_speed*delta_time;
	else if (m_input_manager->is_key_down(SDLK_a))
		m_position.x -= m_speed*delta_time;

	if (m_input_manager->is_key_down(SDLK_1) && m_guns.size() >= 0)
		m_current_gun_index = 0;
	else if (m_input_manager->is_key_down(SDLK_2) && m_guns.size() >= 0)
		m_current_gun_index = 1;
	else if (m_input_manager->is_key_down(SDLK_3) && m_guns.size() >= 0)
		m_current_gun_index = 2;

	glm::vec2 _mouse_coords = m_input_manager->get_mouse_coordinates();
	_mouse_coords = m_camera->convert_screen_to_world(_mouse_coords);

	glm::vec2 _center_pos = m_position + glm::vec2(AGENT_RADIUS);
	m_direction = glm::normalize(_mouse_coords - _center_pos);

	if (m_current_gun_index != -1) 
		m_guns[m_current_gun_index]->update(m_input_manager->is_key_down(SDL_BUTTON_LEFT),
			_center_pos,
			m_direction,
			*m_bullets,
			delta_time);

	collide_with_level(level_data);
}

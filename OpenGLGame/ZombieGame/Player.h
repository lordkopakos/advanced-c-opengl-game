#pragma once

#include"Human.h"
#include"Bullet.h"
#include"Gun.h"

#include<GameEngine\Input_Manager.h>
#include<GameEngine\Camera2D.h>

class Gun; 

class Player :public Human

{
public:
	Player();
	virtual ~Player();
	
	void init(int speed, glm::vec2 position, GameEngine::Input_Manager * input_manager, GameEngine::Camera2D* camera, std::vector<Bullet>* bullets);
	
	void add_gun(std::shared_ptr<Gun> gun);

	void update(const std::vector<std::string>& level_data,
				std::vector<std::shared_ptr<Human>>& humans,
				std::vector<std::shared_ptr<Zombie>>& zombies,
				float delta_time) override;

private:
	std::shared_ptr<GameEngine::Input_Manager> m_input_manager;
	std::shared_ptr<GameEngine::Camera2D> m_camera;
	std::unique_ptr<std::vector<Bullet>> m_bullets;

	std::vector < std::shared_ptr<Gun>> m_guns;
	int m_current_gun_index;
};


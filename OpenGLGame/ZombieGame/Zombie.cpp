#include "Zombie.h"
#include "Human.h"

#include<GameEngine\ResourceManager.h>

Zombie::Zombie()
{
}


Zombie::~Zombie()
{
}

void Zombie::init(float speed, glm::vec2 position)
{
	m_speed = speed;
	m_position = position;

	m_health = 150;

	m_color = GameEngine::ColorRGBA8(255, 255, 255, 255);

	m_texture_ID = GameEngine::ResourceManager::get_texture("Character/Zombie/GreenZombie/green_zombie.png").id;
}

void Zombie::update(const std::vector<std::string>& level_data,
					std::vector<std::shared_ptr<Human>>& humans,
					std::vector<std::shared_ptr<Zombie>>& zombies,
					float delta_time)
{
	std::shared_ptr<Human> _closest_human = get_nearest_human(humans);

	if (_closest_human != nullptr)
	{
		m_direction = glm::normalize(_closest_human->get_position() - m_position);
		m_position += m_direction*m_speed*delta_time;
	}

	collide_with_level(level_data);
}

std::shared_ptr<Human> Zombie::get_nearest_human(std::vector<std::shared_ptr<Human>>& humans)
{
	std::shared_ptr<Human> _closest_human=nullptr;

	float _smallest_distance = 9999999.0f;
	for (const auto & _human : humans)
	{
		glm::vec2 _dis_vec = _human->get_position()-m_position;
		float _distance = glm::length(_dis_vec);

		if (_distance < _smallest_distance)
		{
			_smallest_distance = _distance;
			_closest_human = _human;
		}
	}

	return _closest_human;
}

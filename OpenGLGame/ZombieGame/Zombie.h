#pragma once

#include "Agent.h"

class Zombie :public Agent
{
public:
	Zombie();
	~Zombie();

	void init(float speed, glm::vec2 position);

	virtual void update(const std::vector<std::string>& level_data,
						std::vector<std::shared_ptr<Human>>& humans,
						std::vector<std::shared_ptr<Zombie>>& zombies,
						float delta_time) override;

	private:
		std::shared_ptr<Human > get_nearest_human(std::vector<std::shared_ptr<Human>>& humans);
};

